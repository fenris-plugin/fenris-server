
Fenris 97d (Python)
==============

### Eventos

Evento | Descrição
------ | ---------
player.post           | chamado quando o jogador envia mensagem no post
player.message        | chamado quando o jogador envia mensagem de chat
player.whisper        | chamado quando um jogador envia mensagem privada para outro
player.kill           | chamado quando um jogador mata outro
player.kill_monster   | chamado quando um jogador mata um monstro
player.set            | chamado depois que os atributos do jogador são (re)calculados
player.auth           | chamado quando um jogador realiza login
player.join           | chamado quando um jogador entra no mapa
player.left           | chamado quando um jogador sai do mapa
player.levelup        | chamado quando jogador upa de nivel
player.addpoint       | chamado quando jogador adiciona ponto de status
player.drop_item      | chamado quando o jogador dropa um item no mapa
monster.reload        | chamado quando os monstros são recarregados
monster.kill          | chamado quando um monstro mata um jogador
monster.drop          | chamado quando um item vai dropar de um monstro
timer.tick            | chamado à cada tick do temporizador
player.move           | chamado quando o jogador anda no mapa
npc.talk              | chamado quando jogador acessa um npc

### Protótipos

> player.message (pid_autor : int, mensagem : string) : bool

> player.whisper (pid_autor : int, pid_destinatario : int, mensagem : string) : void

> player.kill (pid_matou : int, pid : morreu) : void

> player.kill_monster (pid_player : int, pid_monster : int) : void

> player.set (pid : int) : void

> player.auth (pid : int, resultado : int) : void

> player.left (pid : int, name : string, account : string) : void

> player.join (pid : int) : void

> player.move (pid : int, map : int, src_x : int, src_y : int, dst_x : int, dst_y : int) : void

> player.addpoint (pid : int, type : int) : bool

> player.drop_item (pid : int, x : int, y : int, pos : int) : bool

> player.use_item (pid : int, source_item : int, target_item : int) : bool

> player.move_to_trade (pid : int, pos : int) : bool

> timer.tick (ticks : int) : void

> monster.kill (pid_monster : int, pid_player : int) : void

> monster.drop (pid_monster : int, pid_player : int) : void

> monster.reload () : void

> npc.talk (pid : int, npc_id : int) : bool

### Observações

- pid = id numérico do jogador/monstro
