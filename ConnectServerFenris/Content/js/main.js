$(document).ready(function() {

	$(".editable").editable({
		url: "/server/edit",
		success: function(data, config) {
			if ($(this).attr('data-name') == 'id') {
				$('.remove[data-id="' + $(this).attr('data-pk') + '"]').attr('data-id', config);
				$(this).attr('data-pk', config);
				$(this).editable('option', 'pk', config);
			}
		}
	});

	$(".editable-visibility").editable({
		url: "/server/edit",
		source: [{"0": "Invis�vel"}, {"1": "Vis�vel"}]
	});

});
