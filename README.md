ATENÇÃO
==============

Viu alguma coisa em algum outro MuServer que não tem aqui? Me adiciona no Skype (wolfezito) e pede que eu coloco aqui sem cobrar absolutamente nada ;)

Fenris 97D
==============

Repositório dos scripts para o GameServer 97d desenvolvido por mim (WoLfulus).
Todos os servidores poderão receber atualizações **oficiais** por esta página do Bitbucket.

Comandos e eventos
==============

- Comando de post
    * /post <mensagem>
- Comando de autopost (mensagem de post automática)
    * /autopost <mensagem>
    * /autopost off
- Comando de premiação com infinitos tipos de eventos **NOVO!**
    * /premiar <nome do personagem> <nome do evento> [pontos]
- Comandos de move custom (infinitos)
    * /move <mapa>
    * /<mapa>
- Sistema de duelo com pontuação 
    * /duelo
    * /aceitar
    * /recusar
- Sistema de pega-pega
    * /iniciapegapega
    * /abrirpegapega
    * /pegapega
    * /fimpegapega
    * /resultadopegapega
- Comando de evento (queroir) definindo mapa, x e y 
    * /abrirevento <mapa> <x> <y>
    * /fecharevento 
    * /queroir
- Comando de contar segundos 
    * /contar <segundos>
- Comandos de PvP
    * *(escrever comandos aqui - completar depois)*
- Mensagens globais (staff) com nome usando `$mensagem`
- Mensagens globais (para todos os servidores ao mesmo tempo) usando `!!mensagem`
    * Obrigatória a utilização do JoinServer Fenris para funcionar.
- Mensagens globais (para todos os servidores ao mesmo tempo) com nome usando `$$mensagem`
    * Obrigatória a utilização do JoinServer Fenris para funcionar.
- __(!)__ Notícia de guilds para todos os servidores ao mesmo tempo
- Notificações de entrada de jogadores e/ou staff
- Comandos de bloqueio de área de PVP
    * /pvp_block
    * /pvp_unblock
- Comando de adicionar pontos 
    * /f <pontos>
    * /a <pontos>
    * /v <pontos>
    * /e <pontos>
- Comando de alterar quantidade de pontos adicionados por clique **NOVO!**
    * /pontos <quantidade>
- Comando de redistribuir pontos **NOVO!**
    * /readd
- Comando de `disconnect` exibido no global
    * /dc <nome>
    * /disconnect <nome>
- Comando de move da `staff` exibe mensagem no global
    * /move <jogador> <mapa> <x> <y>
- Comando + NPCs para reset
    * /reset
- Comando + NPCs para master reset
    * /mreset
- Comando + NPCs para PK clear
    * /pk
- Comando de dar skin para jogador
    * /skin <nome> <monstro>
- Comando de hora
    * /hora
- Comando de trace (move o player pra você)
    * /tr <nome>
- Comando de ir (move você para o player)
    * /ir <nome>
- Comando de zen (seta a quantidade de zen no jogador)
    * /zen <quantidade>
- Comando de info do char 
    * /charinfo <nome>
- Comando para mostrar o baú atual
    * /bau 
- Comando para trocar de baú
    * /bau <bau>
- Comando para mostrar quantos baús a pessoa tem
    * /baus
- Comando para abrir o baú
    * /abrirbau
- Comando de evolução de classe
    * /evo
- Comando de fogos de artifício
    * /fw
- Sistema de box customs (você pode fazer qualquer item se comportar como uma box)
- Sistema de auto-mensagem global
- Sistema de bloqueio de itens no comércio (trade)
- Comandos de ban (personagem e conta)
    * /ban <personagem> <tempo>
    * /banchar <personagem> <tempo>
- Suporte para novos mapas __(TESTES)__
    * Terrain19.att (mapa 18)
    * Terrain20.att (mapa 19)
    * Terrain21.att (mapa 20)
    * Terrain22.att (mapa 21)
    * Terrain23.att (mapa 22)
    * Terrain24.att (mapa 23)
    * Terrain25.att (mapa 24)

__(!)__: Comandos em desenvolvimento

**ATENÇÃO**: todos os comandos podem ser ativados/desativados e configurados como você desejar.

Sistema de Ban
==============

Para utilizar os comandos de ban corretamente, você deverá especificar o tempo do ban. 
Você poderá realizar bans por minutos, horas, dias, semanas, meses ou anos.

Após o tempo do ban, o personagem e/ou a conta será desbloqueado automaticamente pelo MuServer.

###Exemplos

#### Ban de persoangem por 45 minutos
- /banchar WoLfulus 45min

#### Ban de conta por 25 dias
- /ban WoLfulus 25d 

#### Ban de personagem por 3 meses
- /banchar WoLfulus 3m

### Tempos suportados

- Minutos: **min**
- Horas: **h**
- Dias: **d**
- Semanas: **s**
- Meses: **m**
- Anos: **a**

Lembrando que o número e o tempo deverá ser escrito tudo junto. 
**"25 d"** (errado) não é a mesma coisa que **"25d"** (correto)

Sistemas externos
==============

O MuServer conta com um sistema de publish de informações das salas em execução, e é integrada com o ConnectServer do Fenris.

Você pode exibir em tempo real as informações das salas (quantidade de usuários conectados, quantidade máxima, nome do servidor, etc.)

Os dados são atualizados constantemente pelos GameServers na tabela GameServerList.

Para visualizar os dados, basta executar a query no SSMS.

    SELECT * FROM GameServerList

Links
==============

O MuServer conta com links próprios, que fazem com que o MuServer utilize a menor quantidade possível de recursos do computador.

- DataServer próprio
- ConnectServer próprio (com gerenciamento remoto/web)
- JoinServer próprio *(em desenvolvimento)*
- Ranking e Event server desnecessários

Proteções
==============

- Proteção contra speedhack. Nenhum speedhack funcionará mais, pois toda proteção foi realizada no servidor.
- Proteção contra criação de personagens com nick inválido
- Proteção contra ataque dentro da cidade
- Proteção contra DC/Capote
- Proteção contra injection usando mensagens de guild
- Proteção contra flood
- Proteção contra flood `pingback` __(NOVO!)__
- Proteção contra dupe (DevilSquare)
- Proteção contra trade hack (WPE)
- Proteção contra auto-potion

Correções
==============

- Suporte para asa level 1 full __(NOVO!)__
- Muitas outras

Configurações
==============

- Todos os comandos são configuráveis
- Resets acumulativos ou tabelados
- Itens até +11 ou até +15
- Adicional até +16 ou até +28
- 32k ou 65k de status
- Level máximo configurável
- Tempo de drop dos itens
- Tempo para sair `(desconectando em X...)` 
- Máximo de zen no inventário
- Máximo de zen no baú
- Porcentagem de recuperação da apple
- Porcentagem de recuperação das poções pequenas (hp/mana)
- Porcentagem de recuperação das poções médias (hp/mana)
- Porcentagem de recuperação das poções grandes (hp/mana)
- Pontos por nível DK/DW/ELF
- Pontos por nível MG
- Monstro summonado nos orbs
    * Goblin
    * Golem
    * Assassin
    * Yeti
    * Dark Knight
    * Bali
    * Soldier
- Possibilidade de bloquear/liberar pvp nos mapas
- Possibilidade de bloquear/liberar upar nos mapas
- Possibilidade de ativar/desativar reflect (recuo) nos mapas
- Poções infinitas 
- Pets imortais
- Flechas infinitas
- Taxa de defesa do dinorant
- Taxa de bloqueio do dinorant

**Observação** todas as configurações podem ser ativadas/desativadas

Fenris 97D (Client)
==============

#### Correções

- Fixado os crashes aleatórios (ainda pode haver alguns)
- Fixado o problema irritante de acessar o NPC e o cursor do mouse voar para o canto esquerdo superior da tela
- Fixado problema de DC com dinorant (verificando)
- Fixado bug do twisting slash e shield (ataque de longe)
- Fixado a fonte ilegível
- Fixado envio do teclado fora da janela

#### Proteções

- Proteção contra mains falsos (main cracked)
- Verificações simples

#### Customs

- Modo janela que funciona (F9)
- Auto click esquerdo (F8)
- Auto click direito (F7)
- Controle de qualidade dos gráficos (F6)
- Minimizer com traybar 
- Câmera 3D
- Asas level 1 (full)*
- Alguns NPCs novos*

*Fornecido pelo Getulio (Network). Muito obrigado! ~~Só falta sair do bronze!~~ <3

**ATENÇÃO**: o cliente é um **brinde** para os servidores que possuem uma licença do Fenris. 
O mesmo só será publicado quando estiver pronto e devidamente testado, e só funcionará em servidores Fenris.
Dito isso, não tenho nenhuma obrigação de sequer desenvolver o mesmo, e/ou dar datas específicas de publicação do mesmo.
Quando publicado, também não tenho obrigação de manter o mesmo sempre atualização e publicar correções.

**ATENÇÃO 2**: o cliente deverá ser configurado pelo painel de controle do fenris assim que você receber os seus dados de acesso via email.

Recomendações
==============

- Windows Server 2008 R2 SP1
- SQL Server 2008

Requisitos
==============

- [Visual C++ Redistributable 2012 x86](https://www.microsoft.com/en-us/download/details.aspx?id=30679)

Instalação e Tutoriais
==============

1. [Instalação do SQL Server](https://www.youtube.com/watch?v=mPYGTqOWfkg)
2. [Instalação do MuServer](https://www.youtube.com/watch?v=ryVceJgcODk)

Ativação
==============

1. Abrir o GameServer pela primeira vez.
2. Inserir a serial fornecida pelo desenvolvedor e aguardar a ativação do mesmo.

Contato
==============

Único meio de contato é pelo Skype: __*wolfezito*__

Suporte
==============

O suporte oferecido para o projeto Fenris é **exclusivamente a realização de correções e updates do projeto** pelo tempo em que eu 
ainda estiver desenvolvendo o projeto.
Não tenho obrigação de ficar 24h disponível para te ajudar com problemas, nem ajudar você à configurar o seu próprio servidor.
Assumo que as pessoas que contribuem com o projeto adquirindo uma licença Fenris saibam o mínimo necessário para manter um servidor 
rodando, ou se não souberem, que tenham alguém responsável por este tipo de manutenção no servidor.
**Não irei configurar absolutamente nada para você**, e possuir uma licença não te dá o direito de entrar em contato comigo por qualquer
meio que não seja o meu Skype e/ou o fórum do Fenris.

Dúvidas, problemas, solicitações de fixes, alterações e/ou adição de coisas novas, deverão postadas no [fórum do projeto Fenris](http://forum.wolfulus.com).

ATENÇÃO
==============

O projeto foi descontinuado e não é mais mantido/atualizado. Agradeço aos que ajudaram o projeto, mas infelizmente não dá para 
manter isso para sempre. Quem quiser licenças extras e/ou comprar uma licença nova, pode me chamar no Skype, porém, fique ciente 
de que não dou mais nenhum tipo de assistência e/ou atualização: você estará por conta.

[]'s