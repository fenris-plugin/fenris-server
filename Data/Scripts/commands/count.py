# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.timer import *
from utils.config import *

#
# Command
#
class ContarCommand(Command):
	
	# Variaveis
	
	# Constructor
	def initialize(self):
		self.register(config.commands.count, self.command_open)
		self.time = 0
		self.timer = False
		self.players = dict()
		return
		
	# Comando de abrir evento
	def command_open(self, player, arguments):
		if not player.is_admin():
			return True
		if len(arguments) != 1:
			player.message('Uso: %s <tempo em segundos>' % config.commands.count)
			return True
		Server.send_announcement_all('< %s >' % player.get_name())
		Server.send_announcement_all('Preparar...')
		self.time = int(arguments[0]) - 1
		if self.timer != False:
			timer.clear(self.timer)
		self.timer = timer.repeat(self.timer_callback, 1000, int(arguments[0]))
		return True
	
	def timer_callback(self):
		if (self.time == 0):
			Server.send_announcement_all('J�!!')
		else:
			Server.send_announcement_all('%d' % self.time)
			self.time = self.time - 1
		return

#
# Initialization
#
commands.register(ContarCommand())
