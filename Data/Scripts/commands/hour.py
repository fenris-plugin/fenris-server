# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.timer import *
from utils.config import *
from datetime import datetime

#
# Command
#
class CommandHour(Command):

	def initialize(self):
		self.register(config.commands.time, self.command_hour)
		return
	
	def command_hour(self, player, arguments):
		data = datetime.now()
		dias = ('Segunda-feira', 'Ter�a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'S�bado', 'Domingo')
		player.message('Hoje � %02d/%02d/%04d - %02d:%02dh, %s' % (data.day, data.month, data.year, data.hour, data.minute, dias[data.weekday()]))
		return True

#
# Initialization
#
commands.register(CommandHour())