# -*- coding: UTF-8 -*-

#
# Imports
#

from wolfulus import *
from utils.player import *
from utils.chat import * 
from utils.timer import * 
from utils.config import * 

#
# Comando para troca de skins
#
class PkCommand(Command):
	
	#
	# Constructor
    #
	def initialize(self):
		self.npcs = []
		self.register(config.commands.pk_command, self.command_pk)
		Events.register('monster.reload', self.monster_reload)
		Events.register('npc.talk', self.npc_talk)
		timer.timeout(self.init_npc, 5000)
		return
	
	#
	# Monster reload
	#
	def monster_reload(self):
		self.init_npc()
		return

	#
	# Npc
	#
	def init_npc(self):
		self.npcs = []
		for npc in config.data.npcs.pk:
			self.npcs.append(Map(npc[1]).spawn(npc[0], npc[2], npc[3], npc[4]))
		return

	#
	# Npc Talk
	#
	def npc_talk(self, player, npc):
		if self.npcs.count(npc) > 0:
			p = Player(player)
			msg = 'Seu PK foi limpo!'
			if p.get_pklevel() < 3:
				msg = 'Seu Hero foi limpo!'
			if not self.pkclear(p):
				Server.npc_message(player, npc, 'Voce n�o eh nem PK e nem Her�i!')
			else:
				Server.npc_message(player, npc, msg)
			return True
		return False

	#
	# PkClear
	#
	def pkclear(self, player):
		if player.get_pklevel() != 3:
			player.set_pklevel(3)
			player.set_pkcount(0)
			player.set_pktime(0)
			return True
		else:
			return False
	#	
	# Comando de reset
	#
	def command_pk(self, player, arguments):
		msg = 'Seu PK foi limpo!'
		if player.get_pklevel() < 3:
			msg = 'Seu Hero foi limpo!'
		if not self.pkclear(player):
			player.message('Voce n�o eh nem PK nem Her�i!')
		else:
			player.message(msg) 
		return True

#
# Registro global
#
commands.register(PkCommand())

