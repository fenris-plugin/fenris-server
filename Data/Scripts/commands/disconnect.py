# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.config import *

#
# Command
#
class DisconnectCommand(Command):
	
	# Constructor
	def initialize(self):
		self.register(config.commands.disconnect, self.command)
		self.register(config.commands.disconnect_alias, self.command)
		return
	
	#	
	# Comando dc
	#
	def command(self, player, arguments):
		if not player.is_admin():
			return True
		if len(arguments) != 1:
			player.message('Uso: %s <nome>' % config.commands.disconnect)
			return True
		index = Server.find_by_name(arguments[0])
		if (index >= 0):
			target = Player(index)
			if config.systems.dc_global:
				Server.send_announcement_all('[Sistema]');
				Server.send_announcement_all('%s desconectou %s' % (player.get_name(), target.get_name()));
			target.disconnect()
		else:
			player.message('[Sistema] O char informado n�o existe ou n�o est� online.')
		return True

#
# Initialization
#
commands.register(DisconnectCommand())
