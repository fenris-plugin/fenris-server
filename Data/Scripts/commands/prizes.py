# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import * 
import textwrap

#
# Comando para troca de skins
#
class PrizeCommand(Command):

	COLUMN_NAME = 0
	COLUMN_DESCRIPTION = 1
	COLUMN_QUANTITY = 2
	COLUMN_TABLE = 3
	COLUMN_COLUMN = 4
	COLUMN_COLUMN_ID = 5
	COLUMN_TYPE = 6

	# Constructor
	def initialize(self):
		self.register(config.commands.prize, self.command_prize)

		for prize in config.data.prizes.list:
			if prize[PrizeCommand.COLUMN_TYPE].lower() not in ["name", "acc"]:
				raise Exception("Prize type must be either 'name' or 'acc'")

		return
		
	# Comando de premio
	def command_prize(self, player, arguments):
		if not player.is_admin():
			return True

		# Usage
		if len(arguments) < 2 or len(arguments) > 3:
			player.message('Uso: %s <personagem> <evento> [pontos]' % config.commands.prize)
			return True

		# Find prize config
		prize = None
		for p in config.data.prizes.list:
			if p[PrizeCommand.COLUMN_NAME].lower() == arguments[1].lower():
				prize = p
				break

		# Prize not found
		if prize == None:
			prizelist = []
			for p in config.data.prizes.list:
				prizelist.append(p[PrizeCommand.COLUMN_NAME].lower())
			player.message('[Sistema] Evento n�o encontrado. Os eventos registrados s�o: %s' % ', '.join(prizelist))
			return True

		# Find player
		target = Server.find_by_name(arguments[0])
		if not target >= 0:
			player.message('[Sistema] Imposs�vel premiar personagem desconectado.')
			return True
		target = Player(target)

		# Prize quantity
		points = prize[PrizeCommand.COLUMN_QUANTITY]
		if points <= 0:
			if len(arguments) != 3:
				player.message('[Sistema] Voc� precisa especificar a quantidade para este pr�mio.')
				return True
			try:
				points = int(arguments[2])
				if points <= 0:
					raise Exception("Pontos negativos")
			except:
				player.message('[Sistema] Quantidade de pontos deve ser num�rica e maior que zero.')
				return True
		else:
			if len(arguments) >= 3:
				player.message('[Sistema] Este evento utiliza pontos fixos. A quantidade especificada foi ignorada.')

		# Prize query
		value = target.get_name()
		if prize[PrizeCommand.COLUMN_TYPE] == "acc":
			value = target.get_account()
	
		target.inc_db_value_by(prize[PrizeCommand.COLUMN_TABLE], prize[PrizeCommand.COLUMN_COLUMN], points, **{ prize[PrizeCommand.COLUMN_COLUMN_ID]: value })

		Server.send_announcement_all('[SISTEMA]')
		Server.send_announcement_all('%s premiou %s com %d %s' % (player.get_name(), target.get_name(), points, prize[PrizeCommand.COLUMN_DESCRIPTION]))
		return True

		

#
# Registro global
#
commands.register(PrizeCommand())
