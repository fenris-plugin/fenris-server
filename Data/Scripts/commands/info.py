# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.timer import *
from utils.config import *
from datetime import datetime

#
# Command
#
class CommandInfos(Command):

	def initialize(self):
		self.register(config.commands.info, self.command_charinfo)
		return

	def command_charinfo(self, player, arguments):
		if not config.commands.info_enabled:
			player.message('[Sistema] Desculpe, o comando %s est� desabilitado no momento.' % config.commands.info)
			return True

		if config.commands.info_vip:
			if not player.is_vip():
				player.message('[Sistema] Desculpe, somente vips podem utilizar o comando %s.' % config.commands.info)
				return True

		target = player.get_index()
		if len(arguments) == 1:
			target = Server.find_by_name(arguments[0])
		elif len(arguments) > 1:
			player.message('[Sistema] Uso: %s <player>' % config.commands.info)
			return True

		if target != -1:
			target = Player(target)
			player.message('Nome: %s / Resets: %s / MRs: %s / Level: %s' % (arguments[0], target.get_resets(), target.get_mresets(), target.get_level()))
			player.message('F: %s / A: %s / V: %s / E: %s' % (target.get_strength(), target.get_dexterity(), target.get_vitality(), target.get_energy()))
		else:
			player.message('[Sistema] %s n�o esta online nesta sala ou n�o existe.' % arguments[0])
			
		return True

#
# Initialization
#
commands.register(CommandInfos())