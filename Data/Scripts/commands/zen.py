# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.timer import *
from utils.config import *

#
# Command
#
class ZenCommand(Command):
	
	# Variaveis
	
	# Constructor
	def initialize(self):
		self.register(config.commands.zen, self.command_zen)
		return
		
	def command_zen(self, player, arguments):
		if len(arguments) != 1:
			player.message('[Sistema] Uso: %s <quantidade>')
			return True
		if config.commands.zen_vip:
			if not player.is_vip():
				player.message('[Sistema] Você precisa ser VIP para utilizar este comando.')
				return True
		if int(arguments[0]) > config.commands.zen_max:
			player.message('[Sistema] O máximo de zen é 2000000000')
			return True
		else:
			player.set_money(int(arguments[0]))
			player.message('[Sistema] Seu zen foi alterado para %d!' % int(arguments[0]))
		return True

#
# Initialization
#
commands.register(ZenCommand())
