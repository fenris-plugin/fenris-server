# -*- coding: UTF-8 -*-

#
# Imports
#

import sys

from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.timer import *
from utils.config import *
from utils.native import * 
from utils.database import *

from datetime import datetime

#
# Command
#
class CommandWarehouse(Command):

	def initialize(self):
		self.register(config.commands.warehouse, self.command_warehouse)
		self.register(config.commands.warehouses, self.command_warehouses)
		self.register(config.commands.warehouse_open, self.command_warehouse_open)
		return

	def command_warehouse(self, player, arguments):

		if not config.commands.warehouse_enabled:
			player.message('[Sistema] Desculpe, o comando %s est� desabilitado no momento.' % config.commands.warehouse)
			return True

		if config.commands.warehouse_vip:
			if not player.is_vip():
				player.message('[Sistema] Desculpe, somente vips podem utilizar o comando %s.' % config.commands.warehouse)
				return True

		if len(arguments) == 0:
			wid = player.get_current_warehouse_id()
			if wid is None:
				player.message('[Sistema] Erro ao verificar o ID do ba� atual.')
			else:
				player.message('[Sistema] Voc� est� no ba�: ' + str(wid))
			return True
		elif len(arguments) != 1:
			player.message('[Sistema] Uso: %s <numero>' % config.commands.warehouse)
			return True

		if player.get_interface_use() != 0:
			player.message('[Sistema] Voc� n�o pode trocar de ba� no momento.')
			return True

		wnumber = int(arguments[0])
		if wnumber < 0:
			player.message('[Sistema] N�mero inv�lido.')
			return True

		try:
			player.set_interface_use(1)
			db = database.get('muonline')
			cursor = db.cursor()
			cursor.execute("{CALL FENRIS_SWITCH_WAREHOUSE ( ?, ? )}", [player.get_account(), wnumber])		
			row = cursor.fetchone()
			Console.debug(str(row[0]))
			success = int(row[0])
			cursor.close()
			if success == 0:
				player.message('[Sistema] Erro desconhecido ao trocar seu ba�.')
			elif success == 1:
				player.message('[Sistema] Ba� trocado com successo.')
				cursor.commit()
			elif success == 2:
				count = player.get_extra_warehouse_count()
				if count is None:
					player.message('[Sistema] Voc� n�o possui este ba�.')
				else:
					player.message('[Sistema] Desculpe, mas voc� s� possui %d ba�(s).' % (count + 1))
				player.message('Voc� pode adquirir mais ba�s pelo site.')
			else:
				player.message('[Sistema] Erro desconhecido ao trocar seu ba�.')
				player.message('Por favor entre em contato com a administra��o e')
				player.message('informe o c�digo de erro: BAU%d' % success)
				Console.error('WAREHOUSE CHANGE: Unknown error %d' % success)
			return True
		except: 
			player.message('[Sistema] Erro interno ao trocar seu ba�.')
			raise
		finally:
			player.set_interface_use(0)
		
		return True

	def command_warehouses(self, player, arguments):

		if not config.commands.warehouses_enabled:
			player.message('[Sistema] Desculpe, o comando %s est� desabilitado no momento.' % config.commands.warehouses)
			return True

		if len(arguments) != 0:
			player.message('[Sistema] Uso: %s' % config.commands.warehouses)
			return True

		wcount = player.get_extra_warehouse_count()
		if wcount is None:
			player.message('[Sistema] Erro ao verificar quantidade de ba�s.')
			player.message('[Sistema] Voc� j� abriu seu ba� pela primeira vez?')
		else:
			player.message('[Sistema] Voc� possui 1 ba� padr�o + ' + str(wcount) + ' ba�s adicionais.')

		return True

	def command_warehouse_open(self, player, arguments):

		if not config.commands.warehouse_open_enabled:
			player.message('[Sistema] Desculpe, o comando %s est� desabilitado no momento.' % config.commands.warehouse_open)
			return True

		if config.commands.warehouse_open_vip:
			if not player.is_vip():
				player.message('[Sistema] Desculpe, somente vips podem utilizar o comando %s.' % config.commands.warehouse_open)
				return True

		if player.get_interface_use():
			player.message('[Sistema] Voc� n�o pode abrir o ba� no momento.')
			return True

		player.set_interface_use(1)
		player.set_interface_type(6)
		player.set_interface_state(0)
		player.set_target_shop_number(100)
		player.set_shop_time(0)
		player.set_warehouse_count(0)

		gameserver.GDGetWarehouseList(player.get_index(), player.get_address(0x5F))

		return True

#
# Initialization
#
commands.register(CommandWarehouse())