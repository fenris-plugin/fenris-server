# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.timer import *
from utils.config import *

import time

time.time()

#
# Command
#
class PostCommand(Command):
	
	# Variaveis
	
	# Constructor
	def initialize(self):

		self.autos = dict()

		self.register_text(config.commands.post, self.command_post)
		self.register_text(config.commands.autopost, self.command_autopost)

		if config.commands.autopost_interval < config.commands.post_time:
			config.commands.autopost_interval = config.commands.post_time

		if config.commands.autopost_interval < 1:
			config.commands.autopost_interval = 1

		if config.commands.autopost_enabled:
			timer.interval(self.autopost_time, 500)
			Events.register('player.left', self.autopost_left)

	#
	# PLayer left
	#
	def autopost_left(self, player, account, name):
		# Console.write("autopost clear: %d (%s, %s)" % (player, account, name))
		if player in self.autos:
			del self.autos[player]
	
	#
	# Timer
	#
	def autopost_time(self):
		toremove = []
		for auto in self.autos:
			p = Player(auto)
			if not p.is_playing():
				toremove.append(auto)
				continue
			if self.autos[auto]['time'] < time.time():
				self.autos[auto]['time'] = time.time() + config.commands.autopost_interval
				self.command_post(p, self.autos[auto]['message'])

	#
	# Comando de post
	#
	def command_autopost(self, player, text):

		is_vip = player.is_vip()

		text = text.strip()
		if text.lower() == 'off' or text == '':
			if player.get_index() in self.autos:
				player.message('Autopost desativado.')
				self.clear_autopost(player)
			return True

		if not config.commands.autopost_enabled or not config.commands.post_enabled:
			player.message('Desculpe, este comando est� desabilitado no momento.')
			return True

		if config.commands.autopost_vip or config.commands.post_vip:
			if not is_vip:
				player.message('Desculpe mas este comando s� est� dispon�vel para VIPs.')
				return True

		player.message('Autopost ativado.')
		player.message('Mensagem ser�o enviadas � cada %d segundos.' % config.commands.autopost_interval)
	
		self.autos[player.get_index()] = { 'message': text, 'time': time.time() + config.commands.autopost_interval }
		return True

	#
	# Comando de post
	#
	def command_post(self, player, text):
		
		Console.debug('post: %s %s' % (player.get_name(), text))

		is_vip = player.is_vip()

		if config.commands.post_time > 0:
			next_post = player.get_cache('post_time', 0)
			if time.time() < next_post:
				return True
		
		if not config.commands.post_enabled:
			player.message('Desculpe, este comando est� desabilitado no momento.')
			return True

		if config.commands.post_vip:
			if not is_vip:
				player.message('Desculpe mas este comando s� est� dispon�vel para VIPs.')
				self.clear_autopost(player)
				return True

		suffix =''
		if is_vip:
			suffix = '_vip'

		lvl = config.get('commands', 'post_level%s' % suffix)
		if player.get_level() < lvl:
			player.message('Voc� precisa de level %d para usar o post.' % lvl)
			self.clear_autopost(player)
			return True

		zen = config.get('commands', 'post_zen%s' % suffix)
		if zen > 0:
			if player.get_money() < zen:
				player.message('Voc� precisa de %d zen para usar o post.' % zen)
				self.clear_autopost(player)
				return True

		player.set_cache('post_time', time.time() + config.commands.post_time)
		player.set_money(player.get_money() - zen)
		Server.send_message_party_all('[Post]', '%s: %s' % (player.get_name(), text))
		return True

	#
	# Clears the autopost
	#
	def clear_autopost(self, player):
		if player.get_index() in self.autos:
			del self.autos[player.get_index()]

#
# Initialization
#
commands.register(PostCommand())
