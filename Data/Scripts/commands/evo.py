# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.config import *
from utils.native import *

#
# Command
#
class EvoCommand(Command):

	# Constructor
	def initialize(self):
		self.register(config.commands.evo, self.command_evo)
	
	#	
	# Strength
	#
	def command_evo(self, player, arguments):

		if not config.commands.evo_enabled:
			player.message('[Sistema] Este comando est� desabilitado no momento.')
			return True

		if len(arguments) != 0:
			player.message('[Sistema] Uso: %s' % config.commands.evo)
			return True

		if config.commands.evo_vip:
			if not player.is_vip():
				player.message("[Sistema] Este comando est� ativo somente para VIPs.")
				return True

		if player.get_level() < config.commands.evo_level:
			player.message("[Sistema] Voc� precisa de pelomenos level %d para evoluir" % config.commands.evo_level)
			return True
		
		if player.get_money() < config.commands.evo_price:
			player.message("[Sistema] Voc� precisa de %d zen para evoluir" % config.commands.evo_price)
			return True

		if (player.get_dbclass() & 0x07) != 0 or player.get_dbclass() == 48 or player.get_changeup() != 0:
			player.message("[Sistema] Voc� j� est� evoluido")
			return True

		player.set_quest(0, 0xFA)

		player.set_dbclass(player.get_dbclass() + 1)
		player.set_changeup(1)

		cls = (player.get_class() * 32) & 224
		cls = cls | ((player.get_changeup() * 16) & 16)

		gameserver.gObjMakePreviewCharSet(player.get_index())
		gameserver.GCSendQuestPrize(player.get_index(), 201, cls)

		player.set_money(player.get_money() - config.commands.evo_price)

		player.refresh()

		return True

#
# Initialization
#
commands.register(EvoCommand())
