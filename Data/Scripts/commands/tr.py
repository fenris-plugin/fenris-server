# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import * 

#
# Command
#
class TrCommand(Command):

	# Constructor
	def initialize(self):
		self.register(config.commands.trace, self.command)
		return
		
	# Comando move
	def command(self, player, arguments):
		if not player.is_admin():
			return False
		if len(arguments) == 1:
			index = Server.find_by_name(arguments[0])
			if (index >= 0):
				target = Player(index)
				target.teleport(player.get_map(), player.get_x(), player.get_y())
				target.message('[Sistema] Voc� foi movido por %s' % player.get_name())
				player.message('[Sistema] Personagem movido.')
			else:
				player.message('[Sistema] O personagem est� offline ou n�o existe.')
		else:
			player.message('Uso: %s <nome>', config.commands.trace)
			return True
		return True

commands.register(TrCommand())