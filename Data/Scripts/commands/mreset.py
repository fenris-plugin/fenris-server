# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.timer import *

#
# Command
#
class MasterReset(Command):

	def initialize(self):
		self.npcs = []
		self.register(config.commands.mreset, self.command)
		Events.register('npc.talk', self.npc_talk)
		timer.timeout(self.init_npc, 5000)
		return
		
	#
	# Npc
	#
	def init_npc(self):
		self.npcs = []
		for npc in config.data.npcs.mreset:
			self.npcs.append(Map(npc[1]).spawn(npc[0], npc[2], npc[3], npc[4]))
		return

	#
	# Command
	# 
	def command(self, player, arguments):
		self.execute_mreset(player)
		return True
		
	#
	# Npc Talk
	#
	def npc_talk(self, player, npc):
		if self.npcs.count(npc) > 0:
			self.execute_mreset(Player(player))
			return True
		return False

	#
	# Reset
	#
	def execute_mreset(self, player):

		suffix = ''
		if player.is_vip():
			suffix = '_vip'

		lvl = config.get('commands', 'mreset_level%s' % suffix)
		stre = config.get('commands', 'mreset_strength%s' % suffix)
		agi = config.get('commands', 'mreset_agility%s' % suffix)
		vit = config.get('commands', 'mreset_vitality%s' % suffix)
		ene = config.get('commands', 'mreset_energy%s' % suffix)
		rst = config.get('commands', 'mreset_resets%s' % suffix)

		slvl = config.get('commands', 'mreset_status_level%s' % suffix)
		sstr = config.get('commands', 'mreset_status_strength%s' % suffix)
		sagi = config.get('commands', 'mreset_status_agility%s' % suffix)
		svit = config.get('commands', 'mreset_status_vitality%s' % suffix)
		sene = config.get('commands', 'mreset_status_energy%s' % suffix)
		spts = config.get('commands', 'mreset_status_points%s' % suffix)
		smap = config.get('commands', 'mreset_status_map%s' % suffix)
		smapx = config.get('commands', 'mreset_status_map_x%s' % suffix)
		smapy = config.get('commands', 'mreset_status_map_y%s' % suffix)

		if player.get_strength() < stre:
			player.message('Voc� precisa ter %d pontos em for�a!' % stre)
			return

		if player.get_dexterity() < agi:
			player.message('Voc� precisa ter %d pontos em agilidade!' % agi)
			return

		if player.get_vitality() < vit:
			player.message('Voc� precisa ter %d pontos em vitalidade!' % vit)
			return

		if player.get_energy() < ene:
			player.message('Voc� precisa ter %d pontos em energia!' % ene)
			return

		if player.get_level() < lvl:
			player.message('Voc� deve estar pelomenos no level %d!' % lvl)
			return

		if player.get_resets() < rst:
			player.message('Voc� deve ter pelomenos %d resets!' % rst)
			return

		player.inc_mresets()
		player.set_level(slvl)
		player.set_points(spts)
		player.set_exp(0)
		player.set_strength(sstr)
		player.set_dexterity(sagi)
		player.set_vitality(svit)
		player.set_energy(sene)
		player.set_map(smap)
		player.set_x(smapx)
		player.set_y(smapy)
		player.select_character()

#
# Initialization
#
commands.register(MasterReset())

