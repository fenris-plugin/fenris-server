# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.timer import *
from utils.config import *
from utils.events import * 
import random

#
# Evento PvP (Mata-Mata)
#

class PvpEvent:

	# Construtor
	def __init__(self):
		self.isopen = False
		self.players = list()

	# Abre nova arena (time = segundos)
	def open_event(self, time, admin = None):
		
		self.isopen = True
		self.players = list()

		Server.send_announcement_all('[Sistema]')

		# Se o admin n�o for especificado (abertura autom�tica?)
		if admin is None:
			Server.send_announcement_all('Nova arena foi aberta!')
		else:
			Server.send_announcement_all('%s abriu nova arena!' % admin.get_name())

		# Inicializa o timer de abertura do evento 
		timer.repeat(TimerCounter(self.open_timer, time), 1000, time + 1)

	# Timer de abertura/fechamento
	def open_timer(self, time):
		if time == 0:
			self.isopen = False # Fecha a permiss�o de move
			Server.send_announcement_all('Nova arena fechou!')
			# Move os jogadores
			self.move_players()
			# Timeout de 5 segundos para anunciar o in�cio do evento
			timer.timeout(self.start_event, 5000)
		else:
			Server.send_announcement_all('Comando %s fecha em %d segundos.' % (config.commands.tournament_event_join, time))

	# An�ncio do in�cio do evento
	def start_event(self):
		# Aviso aos jogadores
		Server.send_announcement_all('[Sistema]')
		Server.send_announcement_all('Mata-mata come�ar� em %d segundos.' % config.commands.tournament_event_start_time)

		# Executa a fun��o start_event_timer daqui N segundos
		timer.timeout(self.start_event_timer, 1000 * config.commands.tournament_event_start_time)

	# In�cio do evento
	def start_event_timer(self):
		Server.send_announcement_all('Selecionar jogadores...')

	# Move os jogadores que pediram para entrar
	def move_players(self):
		# Para cada jogador na lista
		for player in self.players:
			# Se o jogador estiver jogando (logado, com char selecionado)
			if player.is_playing():
				# Gera uma coordenada entre (x,y) e (x2,y2) para evitar mover os jogadores
				# todos um em cima do outro.
				x = random.randint(config.commands.tournament_event_map_x, config.commands.tournament_event_map_x2)
				y = random.randint(config.commands.tournament_event_map_y, config.commands.tournament_event_map_y2)
				# Teleporta para arena
				player.teleport(config.commands.tournament_event_map, x, y)
				player.message('[Sistema] Bem-vindo � nova arena %s!' % player.get_name())

	# Jogador entra no evento
	def player_join(self, player):
		# Se o evento estiver aberto
		if self.isopen:
			self.players.append(player)
			return True
		else:
			return False



# Instancia o evento
pvp = PvpEvent()


#
# Comando do evento PvP
#

class PvpCommands(Command):

	# Inicializa��o dos comandos
	def initialize(self):
		# Registra o comando de abertura do evento
		self.register(config.commands.tournament_event_open, self.command_open)
		# Registra o comando de entrada do evento
		self.register(config.commands.tournament_event_join, self.command_join)

	# Comando de abertura
	def command_open(self, player, arguments):
		# Se o jogador que executou o comando n�o for admin/gm
		if not player.is_admin():
			# Cancela o comando
			return True

		# Verifica quantidade de parametros e se o primeiro parametro � um n�mero
		if len(arguments) != 1 or not arguments[0].isdigit():
			# Cancela se n�o tiver especificado o tempo, ou tiver valor errado
			player.message('[Sistema] Uso: %s <tempo>' % config.commands.tournament_event_open)
			return True

		# Converte o tempo para n�mero 
		tempo = int(arguments[0])

		# Abre o evento
		pvp.open_event(tempo, player)

	# Comando de entrada do evento
	def command_join(self, player, arguments):

		# Verifica se o evento n�o est� aberto para entrada
		if not pvp.isopen:
			player.message('[Sistema] Nova arena est� fechada no momento.')
			return True

		# Se o jogador que tentou entrar for admin/gm (desativado para testar)
		#if player.is_admin():
		#	player.message('[Sistema] Somente jogadores podem entrar no evento.')
		#	return True

		# Requisi��o de move do jogador
		if pvp.player_join(player):
			player.message('[Sistema] %s, voc� ser� movido assim que a contagem terminar.' % player.get_name())
		else:
			player.message('[Sistema] Desculpe, mas n�o foi poss�vel colocar voc� no evento.')


# Instancia e registra os comandos
pvp_commands = PvpCommands()
commands.register(pvp_commands)
