# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.config import *

#
# Command
#
class StatusCommand(Command):

	# Constructor
	def initialize(self):
		self.register(config.commands.add_strength, self.add_strength)
		self.register(config.commands.add_dexterity, self.add_dexterity)
		self.register(config.commands.add_vitality, self.add_vitality)
		self.register(config.commands.add_energy, self.add_energy)
		self.register(config.commands.readd, self.readd)
		self.register(config.commands.points, self.change_points)
		Events.register('player.addpoint', self.add_point)
		return 
	
	#	
	# Strength
	#
	def add_strength(self, player, arguments):
		if not config.commands.add_enabled:
			player.message('[Sistema] Este comando est� desabilitado no momento.')
			return True
		if len(arguments) != 1:
			player.message('[Sistema] Uso: %s <pontos>' % config.commands.add_strength)
		else:
			points = int(arguments[0])
			if player.get_strength() >= config.commands.add_max:
				player.message('Voc� j� est� com o maximo de pontos for�a!')
				return True
			if player.get_strength() + points > config.commands.add_max:
				points = config.commands.add_max - player.get_strength()
			if player.get_points() < points:
				player.message('[Sistema] Voc� n�o possui esses pontos!')
				return True
			player.set_strength(player.get_strength() + points)
			player.set_points(player.get_points() - points)
			player.message('[Sistema] Pontos adicionados!')
			player.refresh()
		return True

	#	
	# Dexterity
	#
	def add_dexterity(self, player, arguments):
		if not config.commands.add_enabled:
			player.message('[Sistema] Este comando est� desabilitado no momento.')
			return True
		if len(arguments) != 1:
			player.message('[Sistema] Uso: %s <pontos>' % config.commands.add_dexterity)
		else:
			points = int(arguments[0])
			if player.get_dexterity() >= config.commands.add_max:
				player.message('[Sistema] Voc� j� esta com o maximo de pontos em agilidade!')
				return True
			if player.get_dexterity() + points > config.commands.add_max:
				points = config.commands.add_max - player.get_dexterity()
			if player.get_points() < points:
				player.message('[Sistema] Voc� n�o possui esses pontos!')
				return True
			player.set_dexterity(player.get_dexterity() + points)
			player.set_points(player.get_points() - points)
			player.message('[Sistema] Pontos adicionados!')
			player.refresh()
		return True

	#	
	# Vitality
	#
	def add_vitality(self, player, arguments):
		if not config.commands.add_enabled:
			player.message('[Sistema] Este comando est� desabilitado no momento.')
			return True
		if len(arguments) != 1:
			player.message('Uso: %s <pontos>' % config.commands.add_vitality)
		else:
			points = int(arguments[0])
			if player.get_vitality() >= config.commands.add_max:
				player.message('[Sistema] Voc� j� esta com o m�ximo de pontos em vitalidade!')
				return True
			if player.get_vitality() + points > config.commands.add_max:
				points = config.commands.add_max - player.get_vitality()
			if player.get_points() < points:
				player.message('[Sistema] Voc� n�o possui esses pontos!')
				return True
			player.set_vitality(player.get_vitality() + points)
			player.set_points(player.get_points() - points)
			player.message('[Sistema] Pontos adicionados!')
			player.refresh()
		return True

	#	
	# Energy
	#
	def add_energy(self, player, arguments):
		if not config.commands.add_enabled:
			player.message('[Sistema] Este comando est� desabilitado no momento.')
			return True
		if len(arguments) != 1:
			player.message('[Sistema] Uso: %s <pontos>' % config.commands.add_energy)
		else:
			points = int(arguments[0])
			if player.get_energy() >= config.commands.add_max:
				player.message('[Sistema] Voc� j� esta com o m�ximo de pontos em energia!')
				return True
			if player.get_energy() + points > config.commands.add_max:
				points = config.commands.add_max - player.get_energy()
			if player.get_points() < points:
				player.message('[Sistema] Voc� n�o possui esses pontos!')
				return True
			player.set_energy(player.get_energy() + points)
			player.set_points(player.get_points() - points)
			player.message('[Sistema] Pontos adicionados!')
			player.refresh()
		return True

	#
	#
	#
	def change_points(self, player, arguments):
		if len(arguments) != 1:
			player.message('[Sistema] Uso: %s <pontos>' % config.commands.points)
			return True
		points = 1
		try:
			points = int(arguments[0])
			if points <= 0:
				points = 1
			elif points > 100:
				points = 100
		except:			
			player.message('[Sistema] Uso: %s <pontos>' % config.commands.points)
			return True
		player.set_cache('add_quantity', points)
		player.message('[Sistema] Pontos por click alterado para %d' % points)
		return True

	#
	# Readd
	#
	def readd(self, player, arguments):
		if not config.commands.readd_enabled:
			player.message('[Sistema] Este comando est� desabilitado no momento.')
			return True
		if not player.is_vip():
			if config.commands.readd_vip:
				player.message('[Sistema] Este comando est� dispon�vel apenas para VIPs.')
				return True

		points = player.get_strength() + player.get_dexterity() + player.get_energy() + player.get_vitality() + player.get_points()

		if points > config.commands.add_max * 4:
			points = config.commands.add_max * 4

		points = points - 4

		player.set_strength(1)
		player.set_dexterity(1)
		player.set_vitality(1)
		player.set_energy(1)
		player.set_points(points)

		player.message('[Sistema] Pontos removidos! Redistribua como quiser!')
		player.refresh()
		return True
	
	#
	# Manual
	#
	def add_point(self, index, where):

		player = Player(index)
		points = player.get_cache('add_quantity')
		if points == None:
			points = 1

		if where == 0: # Strength
			if player.get_strength() >= config.commands.add_max:
				player.message('[Sistema] Voc� j� atingiu o m�ximo de pontos!')
				return 0
			if player.get_strength() + points > config.commands.add_max:
				points = config.commands.add_max - player.get_strength()

		elif where == 1: # Dexterity
			if player.get_dexterity() >= config.commands.add_max:
				player.message('[Sistema] Voc� j� atingiu o m�ximo de pontos!')
				return 0
			if player.get_dexterity() + points > config.commands.add_max:
				points = config.commands.add_max - player.get_dexterity()

		elif where == 2: # Vitality
			if player.get_vitality() >= config.commands.add_max:
				player.message('[Sistema] Voc� j� atingiu o m�ximo de pontos!')
				return 0
			if player.get_vitality() + points > config.commands.add_max:
				points = config.commands.add_max - player.get_vitality()

		elif where == 3: # Energy
			if player.get_energy() >= config.commands.add_max:
				player.message('[Sistema] Voc� j� atingiu o m�ximo de pontos!')
				return 0
			if player.get_energy() + points > config.commands.add_max:
				points = config.commands.add_max - player.get_energy()
		else:
			return 0

		return points

#
# Initialization
#
commands.register(StatusCommand())
