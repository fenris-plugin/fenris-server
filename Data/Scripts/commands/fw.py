# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import * 

#
# Command
#
class FwCommand(Command):

	# Constructor
	def initialize(self):
		self.register(config.commands.fw, self.command)
		return
		
	# Comando move
	def command(self, player, arguments):
		if not player.is_admin():
			return False
		count = 1
		if len(arguments) == 1:
			count = int(arguments[0])			
		elif len(arguments) > 1:
			player.message('Uso: %s <nome>' % config.commands.fw)
			return True
		if count > 0:
			if count == 1:
				Map(player.get_map()).firework(player.get_x(), player.get_y())
			else:
				Map(player.get_map()).fireworks(player.get_x(), player.get_y(), count)
		return True

commands.register(FwCommand())
