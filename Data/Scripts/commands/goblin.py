# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.monster import *
from utils.chat import *
from utils.timer import *
from utils.config import *
from utils.native import *
from utils.profiler import *

import random
import utils.schedule as schedule

#
# Goblin Event
#
class GoblinEvent(object):

	# 
	# Constructor
	#
	def __init__(self):

		# Armazena o goblin
		self.goblin = None

		# Seguidores do goblin
		self.herd = dict()

		# Diz para o servidor chamar a fun��o toda vez que um item for dropado por um monstro
		Events.register('monster.drop', self.monster_drop)

		# Diz para o servidor chamar a fun��o toda vez que um monstro for morto
		Events.register('player.kill_monster', self.kill_monster)
		
		# Diz para o servidor chamar a fun��o toda vez que um monstro matar algu�m
		Events.register('monster.kill', self.kill_player)

		# Drop a cada 3 segundos
		timer.interval(self.drop_goblin_money, config.goblin_boss.money_interval * 1000)

		# Reposiciona os monstros para ficarem perto do goblin
		timer.interval(self.herd_check, 1000)

		# Spawn do goblin
		for sch in config.data.goblin.schedule:
			if sch[0] == -1:
				schedule.every().day.at('%02d:%02d' % (sch[1], sch[2])).do(self.random_spawn)
			elif sch[0] == 0:
				schedule.every().sunday.at('%02d:%02d' % (sch[1], sch[2])).do(self.random_spawn)
			elif sch[0] == 1:
				schedule.every().monday.at('%02d:%02d' % (sch[1], sch[2])).do(self.random_spawn)
			elif sch[0] == 2:
				schedule.every().tuesday.at('%02d:%02d' % (sch[1], sch[2])).do(self.random_spawn)
			elif sch[0] == 3:
				schedule.every().wednesday.at('%02d:%02d' % (sch[1], sch[2])).do(self.random_spawn)
			elif sch[0] == 4:
				schedule.every().thursday.at('%02d:%02d' % (sch[1], sch[2])).do(self.random_spawn)
			elif sch[0] == 5:
				schedule.every().friday.at('%02d:%02d' % (sch[1], sch[2])).do(self.random_spawn)
			elif sch[0] == 6:
				schedule.every().saturday.at('%02d:%02d' % (sch[1], sch[2])).do(self.random_spawn)
	#
	# Chamado quando um item deve ser dropado de um monstro
	#
	def monster_drop(self, monster_id, player_id):
		if self.goblin is not None:
			if self.goblin.get_index() == monster_id:
				return True
		if monster_id in self.herd:
			self.drop_herd(monster_id, player_id)
			return True
		return False

	#
	# Goblin drop
	#
	def drop_goblin(self, player_id, m, x, y):
		drop = random.choice(config.data.goblin.drops)		
		Map(m).drop_serial(drop[0], drop[1], drop[2], drop[3], x, y, bool(drop[4]), bool(drop[5]), drop[6], drop[7], player_id)

	#
	# Herd drop
	#
	def drop_herd(self, player_id, m, x, y, item):
		Map(m).drop_serial(item.type, item.index, item.level, item.dur, x, y, item.skill, item.luck, item.option, item.excellent, player_id)
			
	#
	# Clear goblin
	#
	def clear_goblin(self):
		Console.debug('Goblin removido.')
		if self.goblin is not None:
			gameserver.gObjDel(self.goblin.get_index())
			del self.goblin
			self.goblin = None

	#
	# Clear goblin
	#
	def clear_herd(self, index):
		Console.debug('Herd %d removido.' % index)
		gameserver.gObjDel(index)
		if index in self.herd:
			del self.herd[index].monster
			del self.herd[index].item
			del self.herd[index]

	#
	# Chamado quando um player matar um monstro
	#
	def kill_monster(self, player_id, monster_id):
		player = Player(player_id)
		monster = Monster(monster_id)
		if self.goblin is not None:
			if self.goblin.get_index() == monster_id:
				Server.send_announcement_all("%s matou o goblin!" % player.get_name())
				Server.send_announcement_all("Parab�ns!")
				timer.timeout(TimerCallback(self.drop_goblin, player_id, self.goblin.get_map(), self.goblin.get_x(), self.goblin.get_y()), 1000)
				self.clear_goblin()
				return

		if monster_id in self.herd:			
			Server.send_announcement_all("%s matou um ajudante do goblin!" % player.get_name())
			timer.timeout(TimerCallback(self.drop_herd, player_id, monster.get_map(), monster.get_x(), monster.get_y(), self.herd[monster_id].item), 1000)
			self.clear_herd(monster_id)
			return

	#
	# Chamado quando um player matar um monstro
	#
	def kill_player(self, monster_id, player_id):
		player = Player(player_id)
		monster = Monster(monster_id)
		if self.goblin is not None:
			if self.goblin.get_index() == monster_id:
				Server.send_announcement_all("%s morreu para um goblin!" % player.get_name())

	#
	# Dropa dinheiro do goblin
	#
	def drop_goblin_money(self):
		if self.goblin is not None:
			self.goblin.drop_money(random.randint(config.goblin_boss.money_min, config.goblin_boss.money_max))

	#
	# Verifica a distancia entre as tropas e o goblin dourado
	#
	def herd_check(self):
		if self.goblin is not None:
			for mon in self.herd:
				if Server.get_distance(mon, self.goblin.get_index()) > 6:
					self.herd[mon].monster.set_mtx(self.goblin.get_x())
					self.herd[mon].monster.set_mty(self.goblin.get_y())

	#
	# Spawna um novo goblin no local especificado
	#
	def random_spawn(self):
		spwn = random.choice(config.data.goblin.spawns)
		if self.spawn(spwn[0], spwn[1], spwn[2]):
			Console.debug('Goblin spawnado em %d (%d, %d)' % (spwn[0], spwn[1], spwn[2]))
			Server.send_announcement_all("[Ca�a ao tesouro]")
			Server.send_announcement_all("Um novo goblin apareceu em %s!!" % spwn[3])
			Server.send_announcement_all("Procurem!!")
		else:
			Console.error('Falha ao spawnar goblin em %d %d %d' % (spwn[0], spwn[1], spwn[2]))

	@profiler
	def close(self):
		if self.goblin is not None:
			Server.send_announcement_all('[Ca�a ao tesouro]')
			Server.send_announcement_all('Os goblins conseguiram fugir! :(')

		self.clear_goblin()

		keys = self.herd.keys()
		for k in keys:
			self.clear_herd(k)
		self.herd = dict()

		#while len(self.herd):
		#	self.clear_herd(self.herd.keys()[0])


	#
	# Spawna um novo goblin no local especificado
	#
	@profiler
	def spawn(self, m, x, y):

		# 78 = Golden goblin

		self.clear_goblin()

		map = Map(m)
		monster = map.spawn(78, x, y, random.randint(0, 7))
		if monster >= 0:

			goblin = Monster(monster)
			goblin.set_level(config.goblin_boss.level)
			goblin.set_life(config.goblin_boss.life)
			goblin.set_mana(config.goblin_boss.mana)
			goblin.set_maxlife(config.goblin_boss.maxlife)
			goblin.set_maxmana(config.goblin_boss.maxmana)
			goblin.set_attackdmgmin(config.goblin_boss.attackdmgmin)
			goblin.set_attackdmgmax(config.goblin_boss.attackdmgmax)
			goblin.set_defense(config.goblin_boss.defense)
			goblin.set_magicdefense(config.goblin_boss.magicdefense)
			goblin.set_attackrate(config.goblin_boss.attackrate)
			goblin.set_blockrate(config.goblin_boss.blockrate)
			goblin.set_moverange(config.goblin_boss.moverange)
			goblin.set_attacktype(config.goblin_boss.attacktype)
			goblin.set_attackrange(config.goblin_boss.attackrange)
			goblin.set_viewrange(config.goblin_boss.viewrange)
			goblin.set_movespeed(config.goblin_boss.movespeed)
			goblin.set_attackspeed(config.goblin_boss.attackspeed)

			self.goblin = goblin

			keys = self.herd.keys()
			for k in keys:
				self.clear_herd(k)
			self.herd = dict()

			for mon in config.data.goblin.herd:

				x = goblin.get_x() + int(mon[0])
				if x < 0:
					x = 0
				elif x > 255:
					x = 255

				y = goblin.get_y() + int(mon[1])
				if y < 0:
					y = 0
				elif y > 255:
					y = 255

				instance_id = map.spawn(mon[2], x, y, random.randint(0, 7))
				if instance_id >= 0:

					monster = Monster(instance_id) 

					monster.set_level(config.goblin_herd.level)
					monster.set_life(config.goblin_herd.life)
					monster.set_mana(config.goblin_herd.mana)
					monster.set_maxlife(config.goblin_herd.maxlife)
					monster.set_maxmana(config.goblin_herd.maxmana)
					monster.set_attackdmgmin(config.goblin_herd.attackdmgmin)
					monster.set_attackdmgmax(config.goblin_herd.attackdmgmax)
					monster.set_defense(config.goblin_herd.defense)
					monster.set_magicdefense(config.goblin_herd.magicdefense)
					monster.set_attackrate(config.goblin_herd.attackrate)
					monster.set_blockrate(config.goblin_herd.blockrate)
					monster.set_moverange(config.goblin_herd.moverange)
					monster.set_attacktype(config.goblin_herd.attacktype)
					monster.set_attackrange(config.goblin_herd.attackrange)
					monster.set_viewrange(config.goblin_herd.viewrange)
					monster.set_movespeed(config.goblin_herd.movespeed)
					monster.set_attackspeed(config.goblin_herd.attackspeed)

					instance = lambda:expando
					instance.id = instance_id
					instance.monster = monster
					instance.item = lambda:expando
					instance.item.type = mon[3]
					instance.item.index = mon[4]
					instance.item.level = mon[5]
					instance.item.dur = mon[6]
					instance.item.skill = bool(mon[7])
					instance.item.luck = bool(mon[8])
					instance.item.option = mon[9]
					instance.item.excellent = mon[10]

					self.herd[instance_id] = instance

			timer.timeout(self.close, 60000 * config.goblin_event.time)

			return True

		return False

# Cria��o do evento
goblin = GoblinEvent()

#
# Goblin commands
#
class GoblinCommands(Command):

	# 
	# Initialization
	#
	def initialize(self):
		self.register(config.commands.goblin, self.goblin_start)

	#
	# Spawna um goblin em uma posi��o aleat�ia
	#
	def goblin_start(self, player, arguments):

		# N�o deixa usu�rios normais usarem o comando
		if not player.is_admin():
			return True

		# Verifica se a quantidade de parametros bate
		if len(arguments) != 3 and len(arguments) != 0:
			player.message('Uso: /%s <mapa> <x> <y>' % config.commands.goblin)
			return True

		if len(arguments) == 3:
			goblin.spawn(int(arguments[0]), int(arguments[1]), int(arguments[2]))
		else:
			goblin.random_spawn()


# Registra o comando para poder ser utilizado
commands.register(GoblinCommands())
