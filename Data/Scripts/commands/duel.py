# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.inventory import * 
from utils.item import * 
from utils.timer import * 
from utils.config import * 
from datetime import datetime
import random
import math

#
# instancia do duelo
#
class Duel:

	# constructor
	def __init__(self, player1, player2):
		self.points = dict()
		self.players = [player1, player2]		
		self.player_names = [player1.get_name(), player2.get_name()]
		self.points[player1.get_name()] = 0
		self.points[player2.get_name()] = 0
		self.finish_callback = None
		timer.timeout(self.start, 5000)
		self.respawn_id = None
		self.respawn_map = player2.get_map()
		self.respawn_x = player2.get_x()
		self.respawn_y = player2.get_y()
		self.duel_time = config.commands.duel_time
		player1.message('[Sistema] O duelo come�ar� em 5 segundos...')
		player1.message('[Sistema] Tempo m�ximo do duelo: %d minutos' % self.duel_time)
		player2.message('[Sistema] O duelo come�ar� em 5 segundos...')
		player2.message('[Sistema] Tempo m�ximo do duelo: %d minutos' % self.duel_time)
		self.check_timer = timer.interval(self.check, 1000)
		self.cancelled = False
		self.cancelled_message = ''
		self.cancelled_winner = None
		self.cancelled_looser = None
		self.timer = False
		
		
	def respawn(self):
		if self.respawn_id != None:
			timer.clear(self.respawn_id)
			self.respawn_id = None
		self.respawn_id = timer.timeout(self.respawn_callback, 1000 * config.commands.duel_respawn_time)
		
	def respawn_callback(self):
		self.respawn_id = None
		self.players[0].teleport(self.respawn_map, self.respawn_x, self.respawn_y)
		self.players[1].teleport(self.respawn_map, self.respawn_x, self.respawn_y)
		self.players[0].message('[Sistema] Lutem!')
		self.players[1].message('[Sistema] Lutem!')
		
	def start(self):
		tempo = (1000 * 60) * self.duel_time		
		self.timer = timer.timeout(self.finish, tempo)
		self.respawn_callback()

	def clear_timer(self):
		if self.timer != False:
			timer.clear(self.timer)

	def finish(self):
	
		timer.clear(self.check_timer)
	
		if self.check_timer != None:
			timer.clear(self.check_timer)
			self.check_timer = None
			
		cancels = 0
		
		if not self.players[0].is_playing():
			self.cancelled = True
			self.cancelled_message = '%s se desconectou.' % self.player_names[0]
			self.cancelled_winner = self.players[1]
			self.cancelled_looser = self.players[0]
			cancels = cancels + 1
		
		if not self.players[1].is_playing():
			self.cancelled = True
			self.cancelled_message = '%s se desconectou.' % self.player_names[1]
			self.cancelled_winner = self.players[0]
			self.cancelled_looser = self.players[1]
			cancels = cancels + 1
			
		if cancels >= 2:
			# os dois jogadores sairam, entao encerra sem fazer nada.
			if self.finish_callback != None:
				self.finish_callback(self, self.cancelled_winner, self.cancelled_looser)
			self.clear_timer()
			return
			
		self.players[0].message('[Sistema] Duelo encerrado.')
		self.players[1].message('[Sistema] Duelo encerrado.')
		
		player1 = self.players[0]
		player2 = self.players[1]
		
		if self.cancelled:
			self.players[0].message('[Sistema] Resultado: %s ganhou.' % self.cancelled_winner.get_name())
			self.players[0].message('[Sistema] Motivo: %s' % self.cancelled_message)
			self.players[1].message('[Sistema] Resultado: %s ganhou.' % self.cancelled_winner.get_name())
			self.players[1].message('[Sistema] Motivo: %s' % self.cancelled_message)
			if self.finish_callback != None:
				self.finish_callback(self, self.cancelled_winner, self.cancelled_looser)
			self.clear_timer()
			return 
		
		pontos1 = self.points[self.player_names[0]]
		pontos2 = self.points[self.player_names[1]]
		
		if pontos1 == pontos2:			
			self.players[0].message('[Sistema] Resultado: empatado com %d pontos' % pontos1)
			self.players[1].message('[Sistema] Resultado: empatado com %d pontos' % pontos1)
			if self.finish_callback != None:
				self.finish_callback(self, None, None)
			self.clear_timer()
		else:
			pontos = pontos1
			winner = player1
			looser = player2
			if pontos2 > pontos1:
				pontos = pontos2
				winner = player2
				looser = player1		
			self.players[0].message('[Sistema] Resultado: %s venceu com %d pontos' % (winner.get_name(), pontos))
			self.players[1].message('[Sistema] Resultado: %s venceu com %d pontos' % (winner.get_name(), pontos))
			if self.finish_callback != None:
				self.finish_callback(self, winner, looser)
			self.clear_timer()
		
	def on_finish(self, callback):
		self.finish_callback = callback
		
	def event_kill(self, id1, id2):
		killer = Player(id1)
		victim = Player(id2)	
		
		if not (killer.get_name() == self.players[0].get_name() and victim.get_name() == self.players[1].get_name()):
			if not (victim.get_name() == self.players[0].get_name() and killer.get_name() == self.players[1].get_name()):
				return
		
		if self.respawn_id != None:			
			killer.message('[Sistema] N�o valeu! Aguarde o sinal.')
			victim.message('[Sistema] N�o valeu! %s n�o aguardou o sinal.' % killer.get_name())
			self.respawn()
			return
		
		pontos = self.points[killer.get_name()]
		pontos = pontos + 1
		self.points[killer.get_name()] = pontos
				
		victim.message('[Sistema] %s marcou um ponto' % killer.get_name())
		killer.message('[Sistema] Voc� marcou um ponto')	

		if pontos >= 3:
			self.finish()
		else:
			self.respawn()
		
	def cancel(self, message, winner, looser):
		self.cancelled = True
		self.cancelled_message = message
		self.cancelled_winner = winner
		self.cancelled_looser = looser
		self.finish()
	
	def distance(self, x, y, x2, y2):
		return math.sqrt((x - x2) ** 2 + (y - y2) ** 2)
		
	def check(self):
		if not self.players[0].is_playing():
			self.cancel('%s se desconectou.' % self.player_names[0], self.players[1], self.players[0])
			return
		if not self.players[1].is_playing():
			self.cancel('%s se desconectou.' % self.player_names[1], self.players[0], self.players[1])
			return
		if self.players[0].get_map() != config.commands.duel_map:
			self.cancel('%s saiu da arena.' % self.player_names[0], self.players[1], self.players[0])
			return
		if self.players[1].get_map() != config.commands.duel_map:
			self.cancel('%s saiu da arena.' % self.player_names[1], self.players[0], self.players[1])
			return
		if self.distance(self.players[0].get_x(), self.players[0].get_y(), self.respawn_x, self.respawn_y) > config.commands.duel_distance:
			self.cancel('%s se distanciou da area de combate.' % self.player_names[0], self.players[1], self.players[0])
			return 
		if self.distance(self.players[1].get_x(), self.players[1].get_y(), self.respawn_x, self.respawn_y) > config.commands.duel_distance:
			self.cancel('%s se distanciou da area de combate.' % self.player_names[1], self.players[0], self.players[1])
			return 
		
class DuelRequest:

	def __init__(self, source, target):
		self.source = source
		self.source_name = source.get_name() 
		self.target = target
		self.target_name = target.get_name()
		self.timeout_id = timer.timeout(self.timeout, 1000 * 10) # 10 segundos
		self.timeout_callback = None
		
	# pega o jogador que pediu o duel
	def get_source(self):
		return self.source
	def get_source_name(self):
		return self.source_name
		
	# pega o jogador que recebeu o pedido
	def get_target(self):
		return self.target
	def get_target_name(self):
		return self.target_name
		
	def on_timeout(self, callback):
		self.timeout_callback = callback
		
	def clear_timeout(self):
		if self.timeout_id != None:
			timer.clear(self.timeout_id)
			self.timeout_id = None
			
	def timeout(self):
		if self.timeout_callback != None:
			self.timeout_callback(self)
		
		
#
# command
#	
class DuelCommand(Command):
	
	def initialize(self):
		self.duels = []
		self.requests = dict() 
		self.register(config.commands.duel_request, self.command_duel)
		self.register(config.commands.duel_accept, self.command_accept)
		self.register(config.commands.duel_reject, self.command_refuse)
		Events.register('player.kill', self.event_kill)
		return
		
	def command_accept(self, player, arguments):
		target = player
		
		if len(arguments) != 1:
			player.message('[Sistema] Uso: %s <nome>' % config.commands.duel_accept)
			return True
			
		if arguments[0] in self.requests:
			request = self.requests[arguments[0]]
			if request.get_target_name() != target.get_name():
				target.message('[Sistema] Este jogador n�o pediu duelo com voce.')
				return True			
			
			source = Server.find_by_name(arguments[0])
			if source < 0:
				target.message('[Sistema] Este jogador est� offline ou n�o existe.')
				self.requests[arguments[0]].clear_timeout()
				del self.requests[arguments[0]]
				return True
				
			source = Player(source)		
			
			source.message('[Sistema] Duelo aceito. Teleportando voce para o jogador.')
			source.teleport(target.get_map(), target.get_x(), target.get_y())
			
			self.requests[arguments[0]].clear_timeout()
			del self.requests[arguments[0]]
			
			names_to_delete = []
			for name in self.requests:
				if self.requests[name].get_target_name() == target.get_name():
					self.requests[name].get_source().message('[Sistema] Desculpe, mas o jogador %s aceitou o duelo com outra pessoa.' % target.get_name())
					self.requests[name].clear_timeout()
					names_to_delete.append(name)
			
			for name in names_to_delete:
				del self.requests[name]		
										
			duel = Duel(source, target)
			duel.on_finish(self.duel_finish)
			self.duels.append(duel)
			
		else:
			player.message('[Sistema] Este jogador não pediu duelo com voce.')
			return True	
			
		return True
		
	def duel_finish(self, duel, winner, looser):
		self.remove_duel(duel)
		if winner != None:
			winner.inc_duel_wins()
		if looser != None:
			looser.inc_duel_losses()
		
	def remove_duel(self, duel):
		for key in range(len(self.duels)):
			d = self.duels[key]
			if d.players[0].get_name() == duel.players[0].get_name() and d.players[1].get_name() == duel.players[1].get_name():
				del self.duels[key]
				return
		
	def command_refuse(self, player, arguments):
			
		target = player
		
		if len(arguments) != 1:
			player.message('[Sistema] Uso: %s <nome>' % config.commands.duel_reject)
			return True
			
		if arguments[0] in self.requests:
			request = self.requests[arguments[0]]
			if request.get_target_name() != target.get_name():
				target.message('[Sistema] Este jogador não pediu duelo com voce.')
				return True			
			
			source = Server.find_by_name(arguments[0])
			if source < 0:
				target.message('[Sistema] Este jogador está offline ou não existe.')
				self.requests[arguments[0]].clear_timeout()
				del self.requests[arguments[0]]
				return True
				
			source = Player(source)		
			
			source.message('[Sistema] %s recusou o seu pedido de duelo' % target.get_name())
			target.message('[Sistema] Voce recusou o duelo contra %s' % source.get_name())
						
			self.requests[source.get_name()].clear_timeout()
			del self.requests[source.get_name()]
			
		else:
			player.message('[Sistema] Este jogador não pediu duelo com voce.')
			return True	
			
		return True
		
	def command_duel(self, player, arguments):		
		if player.get_name() in self.requests:
			player.message('[Sistema] Pedido para %s ainda está pendente. Aguarde.' % self.requests[player.get_name()].get_target_name())
			return True
			
		if len(arguments) != 1:
			player.message('[Sistema] Uso: %s <nome>' % config.commands.duel_request)
			return True
			
		target = Server.find_by_name(arguments[0])
		if target < 0:
			player.message('[Sistema] Este jogador está offline ou não existe.')
			return True
			
		target = Player(target)		
		if player.get_name() == target.get_name():
			player.message('[Sistema] Voce não pode pedir duelo pra si mesmo.')
			return True
		
		if target.get_name() in self.requests:
			player.message('[Sistema] Este jogador está tentando duelar contra outra pessoa.')
			return True
			
		if self.in_duel(target.get_name()):
			player.message('[Sistema] Este jogador ja está duelando com outra pessoa.')
			return True
				
		if player.get_map() != config.commands.duel_map:
			player.message('[Sistema] Voc� deve estar no mapa certo para poder duelar.')
			return True
			
		if target.get_map() != config.commands.duel_map:
			player.message('[Sistema] O jogador deve estar no mapa certo para poder duelar.')
			return True
			
		if Server.get_distance(player.get_index(), target.get_index()) > config.commands.duel_distance:
			player.message('[Sistema] Voce deve estar perto do jogador para pedir duelo.')
			return True
			
		target.message('[Sistema] %s enviou um pedido de duelo.' % player.get_name())
		target.message('[Sistema] Digite "%s %s" para aceitar.' % (config.commands.duel_accept, player.get_name()))
		target.message('[Sistema] Digite "%s %s" para recusar ou aguarde 10 segundos.' % (config.commands.duel_reject, player.get_name()))
		player.message('[Sistema] Pedido enviado para %s' % target.get_name())
		
		request = DuelRequest(player, target)
		request.on_timeout(self.request_timeout)
		self.requests[player.get_name()] = request
		return True
		
	def in_duel(self, name):
		for duel in self.duels:
			if (duel.players[0].get_name() == name or duel.players[1].get_name() == name):
				return True
		return False
			
	def request_timeout(self, request):
		target = request.get_target()
		target_name = request.get_target_name()
		source = request.get_source()
		source_name = request.get_source_name()
		if source_name in self.requests:
			source.message('[Sistema] Seu pedido de duelo contra %s expirou.' % target_name)
			target.message('[Sistema] Pedido de duelo de %s expirou.' % source_name)
			del self.requests[source_name]
			
	def event_kill(self, id1, id2):
		for key in range(len(self.duels)):
			self.duels[key].event_kill(id1, id2)

#
# Initialization
#
commands.register(DuelCommand())
