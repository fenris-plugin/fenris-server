# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import * 
from utils.timer import * 
from utils.config import * 

class ResetConfig:

	def __init__(self, vip, level, setlevel, status):
		self.vip = vip
		self.status = status
		self.level = level
		self.setlevel = setlevel
		self.points = []

	def init(self):
		if len(self.points) <= 0:
			raise Exception("Reset tabelado sem informaçăo de tabela para VIP." % self.vip)
		for i in range(len(self.points)):
			if i == 0:
				self.points[i].basepoints = 0
			else:
				self.points[i].basepoints = self.points[i-1].basepoints + (self.points[i].resets - self.points[i-1].resets) * self.points[i-1].points

	def get_config(self, resets):
		for i in reversed(range(len(self.points))):
			if resets >= self.points[i].resets:
				return self.points[i]
		raise Exception('Impossivel determinar configuraçăo de pontos')

	def get_points(self, resets):        
		for i in reversed(range(len(self.points))):
			if resets >= self.points[i].resets:
				return self.points[i].basepoints + self.points[i].points * ((resets + 1) - self.points[i].resets)
		raise Exception('Impossivel determinar quantidade de pontos')


class ResetPoints:

	def __init__(self, resets, points, price):
		self.resets = resets
		self.points = points
		self.price = price
		self.basepoints = 0


#
# Comando para troca de skins
#
class ResetCommand(Command):
	
	#
	# Constructor
    #
	def initialize(self):
		self.npcs = []
		self.register(config.commands.reset, self.command_reset)
		Events.register('monster.reload', self.monster_reload)
		Events.register('npc.talk', self.npc_talk)
		timer.timeout(self.init_npc, 5000)

		self.table = []

		for cfg in config.data.resets.configs:
			conf = ResetConfig(int(cfg[0]), int(cfg[1]), int(cfg[2]), int(cfg[3]))
			for pts in config.data.resets.points:
				if int(pts[0]) == conf.vip:
					conf.points.append(ResetPoints(int(pts[1]), int(pts[2]), int(pts[3])))
			conf.points.sort(key=lambda x: x.resets)
			conf.init()
			self.table.append(conf)

		self.table.sort(key=lambda x: x.vip)

		if not config.commands.reset_accumulative:
			if len(self.table) <= 0:
				Server.close_error('O servidor está configurado para reset pontuativo mas năo possui configuraçőes de tabela.')
				raise Exception("Falha nas configuraçőes de reset pontuativo")

		for p in self.table:
			if len(p.points) <= 0:
				Server.close_error('O servidor possui configuraçőes level e status para o VIP tipo %d mas este vip năo está na lista de configuraçőes pontos.' % p.vip)
				raise Exception("Falha nas configuraçőes de reset pontuativo")
			if p.points[0].resets != 0:
				Server.close_error('A primeira entrada da tabela de resets deve sempre começar com 0 resets.')
				raise Exception("Falha nas configuraçőes de reset pontuativo")

		for p in config.data.resets.points:
			found = False
			for p2 in self.table:
				if p2.vip == int(p[0]):
					found = True
			if found == False:
				Server.close_error('O servidor possui configuraçőes de pontos para o VIP tipo %d mas este vip năo está na lista de configuraçőes de level/status.' % (int(p[0])))
				raise Exception("Falha nas configuraçőes de reset pontuativo")

	#
	# Monster reload
	#
	def monster_reload(self):
		self.init_npc()
		return

	#
	# Npc
	#
	def init_npc(self):
		self.npcs = []
		for npc in config.data.npcs.reset:
			self.npcs.append(Map(npc[1]).spawn(npc[0], npc[2], npc[3], npc[4]))
		return

	#
	# Npc Talk
	#
	def npc_talk(self, player, npc):
		if self.npcs.count(npc) > 0:
			p = Player(player)
			if not self.reset(p):
				Server.npc_message(player, npc, 'Impossível resetar!')
			else:
				Server.npc_message(player, npc, 'Parabéns, vocę resetou!')
			return True
		return False

	#
	# Resets o personagem
	#
	def reset(self, player):
		if config.commands.reset_accumulative:
			return self.reset_accumulative(player)
		else:
			return self.reset_points(player)


	def reset_points(self, player):

		vip = player.is_vip()
		if vip is False:
			player.message('[Sistema] Erro ao resetar. Impossível definir seu status VIP.')
			Console.error('Impossivel resetar personagem. Status VIP do personagem %s indefinido.' % player.get_name())
			return False

		resets = player.get_resets()
		if resets is None or resets is False:
			player.message('[Sistema] Erro ao resetar. Impossível definir seus resets.')
			Console.error('Impossivel resetar personagem. Resets do personagem %s indefinido.' % player.get_name())
			return False

		cfg = None
		for x in range(len(self.table)):
			if vip == self.table[x].vip:
				cfg = self.table[x]
				break
			if vip < self.table[x].vip:
				break
			cfg = self.table[x]			

		if cfg is None:
			player.message('[Sistema] Erro ao resetar. Por favor entre em contato com o administrador.')
			Console.error('Erro ao realizar reset. Configuracao para VIP %d nao encontrada.' % vip)
			return False

		if player.get_level() < cfg.level:
			player.message('Vocę precisa de level %d para resetar.' % cfg.level)
			return False

		rconfig = cfg.get_config(resets)
		rpoints = cfg.get_points(resets)

		if player.get_money() < rconfig.price:
			player.message('Vocę precisa de %d zen para resetar.' % rconfig.price)
			return False

		check_items = False
		if vip and config.commands.reset_remove_items_vip:
			check_items = True
		elif not vip and config.commands.reset_remove_items:
			check_items = True

		if check_items:
			inv = player.get_inventory()
			for i in range(12):
				if inv.has_item(i):
					player.message('Vocę precisa remover os itens para resetar.')
					return False

		player.inc_resets()
		
		player.set_level(cfg.setlevel)
		player.set_exp(0)
		player.set_money(player.get_money() - rconfig.price)
		player.set_points(rpoints)
		player.set_strength(cfg.status)
		player.set_dexterity(cfg.status)
		player.set_vitality(cfg.status)
		player.set_energy(cfg.status)

		player.set_map(0)
		player.set_x(125)
		player.set_y(125)

		player.select_character()

		return True

	def reset_accumulative(self, player):

		vip = False
		suffix = ''
		if player.is_vip():
			vip = True
			suffix = '_vip'

		level = int(config.get('commands', 'reset_level%s' % suffix))
		if player.get_level() < level:
			player.message('Vocę precisa de level %d para resetar.' % level)
			return False

		zen = int(config.get('commands', 'reset_zen%s' % suffix))
		if player.get_money() < zen:
			player.message('Vocę precisa de %d zen para resetar.' % zen)
			return False

		slevel = int(config.get('commands', 'reset_set_level%s' % suffix))
		exp = int(config.get('commands', 'reset_set_exp%s' % suffix))
		repos = int(config.get('commands', 'reset_set_pos%s' % suffix))
		mapn = int(config.get('commands', 'reset_set_map%s' % suffix))
		mapx = int(config.get('commands', 'reset_set_map_x%s' % suffix))
		mapy = int(config.get('commands', 'reset_set_map_y%s' % suffix))

		check_items = False
		if vip and config.commands.reset_remove_items_vip:
			check_items = True
		elif not vip and config.commands.reset_remove_items:
			check_items = True

		if check_items:
			inv = player.get_inventory()
			for i in range(12):
				if inv.has_item(i):
					player.message('Vocę precisa remover os itens para resetar.')
					return False
					
		player.inc_resets()
		
		player.set_level(slevel)
		player.set_exp(exp)
		player.set_money(player.get_money() - zen)

		if repos:
			player.set_map(mapn)
			player.set_x(mapx)
			player.set_y(mapy)

		player.select_character()
		return True
	
	#	
	# Comando de reset
	#
	def command_reset(self, player, arguments):
		if not self.reset(player):
			player.message('Impossível resetar!')
		else:
			player.message('Parabéns, vocę resetou!') 
		return True

#
# Registro global
#
commands.register(ResetCommand())
