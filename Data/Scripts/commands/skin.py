# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import * 

#
# Comando para troca de skins
#
class SkinCommand(Command):
	
	# Constructor
	def initialize(self):
		self.register(config.commands.skin, self.command_skin)
		return
		
	# Comando de skin
	def command_skin(self, player, arguments):
		if not player.is_admin():
			return True
		if len(arguments) != 2:
			player.message('Uso: %s <nome> <monstro>' % config.commands.skin)
			return True
		index = Server.find_by_name(arguments[0])
		if (index >= 0):
			target = Player(index)
			target.set_skin(int(arguments[1]))
			target.message('[Sistema] Voc� recebeu uma skin de %s' % player.get_name())
			player.message('[Sistema] Personagem transformado.')
		else:
			player.message('[Sistema] O personagem est� offline ou n�o existe.')
		return True

#
# Registro global
#
commands.register(SkinCommand())
