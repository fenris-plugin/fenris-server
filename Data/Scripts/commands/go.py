# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import * 

#
# Command
#
class GoCommand(Command):

	# Constructor
	def initialize(self):
		self.register(config.commands.go, self.command)
		return
		
	# Comando move
	def command(self, player, arguments):
		if not player.is_admin():
			return False
		if len(arguments) == 1:
			index = Server.find_by_name(arguments[0])
			if (index >= 0):
				target = Player(index)
				player.teleport(target.get_map(), target.get_x(), target.get_y())
				player.message('[Sistema] Voc� foi movido at� %s' % target.get_name())
			else:
				player.message('[Sistema] O personagem est� offline ou n�o existe.')
		else:
			player.message('Uso: %s <nome>' % config.commands.go)
			return True
		return True

commands.register(GoCommand())
