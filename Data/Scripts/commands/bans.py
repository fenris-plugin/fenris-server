# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.timer import *
from utils.config import *
from utils.database import *

import re

#
# Command
#
class BanCommand(Command):

	# Constructor
	def initialize(self):
		self.compiled = re.compile('(\\d+)([a-z]+)', re.IGNORECASE | re.DOTALL)
		self.register(config.commands.ban, self.command_ban)
		self.register(config.commands.banchar, self.command_banchar)
		return

	# Extrai o tempo utilizado no comando
	def extract_time(self, time):
		try:
			r = self.compiled.search(time)
			if not r:
				return None
				
			v = int(r.group(1))
			t = str(r.group(2))
			
			rs = {
				'min': (1, 'minutos' if v > 1 else 'minuto'), 
				'h': (60, 'horas' if v > 1 else 'hora'), 
				'd': (1440, 'dias' if v > 1 else 'dia'), 
				's': (10080, 'semanas' if v > 1 else 'semana'), 
				'm': (43200, 'meses' if v > 1 else 'mes'), 
				'a': (525600, 'anos' if v > 1 else 'ano')
			}
			
			if t in rs:
				r = rs[t]
				return (r[0] * v, '%d %s' % (v, r[1]))
			
		except:
			return None
			
		return None

	# Comando de abrir evento
	def command_ban(self, player, arguments):

		if not player.is_admin():
			return True

		if not config.commands.ban_enabled:
			player.message('[Sistema] Este comando est� desabilitado.')
			return True

		if len(arguments) != 2:
			player.message('[Sistema] Uso: %s <personagem> <tempo>' % config.commands.ban)
			return True

		time = self.extract_time(arguments[1])
		if time is None:
			player.message('[Sistema] Formato de tempo inv�lido.')
			player.message('Consulte o manual para saber como utilizar corretamente este comando.')
			return True

		target = Server.find_by_name(arguments[0])
		if target == -1:
			player.message('Personagem n�o existe ou est� offline.')
			return True

		target = Player(target)

		if target.is_admin():
			player.message('Voc� n�o pode banir um admin/gm.')
			return True

		minutes = time[0]
		description = time[1]

		db = database.get('muonline')
		cursor = db.cursor()
		cursor.execute('UPDATE MEMB_INFO SET bloc_code = 1, %s = DATEADD(MINUTE, ?, GETDATE()) WHERE memb___id = ?' % (config.columns.ban_timer), [minutes, target.get_account()])
		cursor.close()
		db.commit()

		Server.send_announcement_all('[SISTEMA]')
		Server.send_announcement_all('%s baniu a conta de %s por %s' % (player.get_name(), target.get_name(), description))

		target.set_closetype(2)
		target.set_closecount(6)

		return True

	# Comando de fechar evento
	def command_banchar(self, player, arguments):

		if not player.is_admin():
			return True

		if not config.commands.ban_enabled:
			player.message('[Sistema] Este comando est� desabilitado.')
			return True

		if len(arguments) != 2:
			player.message('[Sistema] Uso: %s <personagem> <tempo>' % config.commands.banchar)
			return True

		time = self.extract_time(arguments[1])
		if time is None:
			player.message('[Sistema] Formato de tempo inv�lido.')
			player.message('Consulte o manual para saber como utilizar corretamente este comando.')
			return True

		target = Server.find_by_name(arguments[0])
		if target == -1:
			player.message('Personagem n�o existe ou est� offline.')
			return True

		target = Player(target)

		if target.is_admin():
			player.message('Voc� n�o pode banir um admin/gm.')
			return True

		minutes = time[0]
		description = time[1]

		db = database.get('muonline')
		cursor = db.cursor()
		cursor.execute('UPDATE Character SET CtlCode = 1, %s = DATEADD(MINUTE, ?, GETDATE()) WHERE Name = ?' % (config.columns.banchar_timer), [minutes, target.get_name()])
		cursor.close()
		db.commit()

		Server.send_announcement_all('[SISTEMA]')
		Server.send_announcement_all('%s baniu %s por %s' % (player.get_name(), target.get_name(), description))

		target.set_closetype(1)
		target.set_closecount(6)

		return True

#
# Initialization
#
commands.register(BanCommand())
