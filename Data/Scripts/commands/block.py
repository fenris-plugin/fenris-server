# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.chat import *
from utils.timer import *
from utils.config import *

#
# Command
#
class BlockCommand(Command):

	# Constructor
	def initialize(self):
		self.register(config.commands.pvp_area_block, self.command_abrir)
		self.register(config.commands.pvp_area_unblock, self.command_fechar)
		Events.register('player.move', self.on_move)
		timer.interval(self.pvp_check, 1000)
		self.players = dict()
		self.avisados = []
		self.open = False
		return

	# Verifica se ta dentro da area de pvp
	def pvp_area(self, map, x, y):

		m = config.commands.pvp_area_map

		x1 = config.commands.pvp_area_block_x1
		y1 = config.commands.pvp_area_block_y1
		x2 = config.commands.pvp_area_block_x2
		y2 = config.commands.pvp_area_block_y2

		fx1 = config.commands.pvp_area_free_x1
		fy1 = config.commands.pvp_area_free_y1
		fx2 = config.commands.pvp_area_free_x2
		fy2 = config.commands.pvp_area_free_y2

		if map == m:
			if (x >= x1 and y >= y1) and (x <= x2 and y <= y2):
				if (x >= fx1 and y >= fy1) and (x <= fx2 and y <= fy2):
					return False
				return True
		return False

	# Timer de checagem pvp
	def pvp_check(self):
		for index in self.players.keys():
			player = Player(index)
			time = self.players[index]
			
			if not player.is_playing():
				del self.players[index]
				continue

			if player.is_admin():
				del self.players[index]
				continue

			if time > 0:
				player.message('[Sistema] Voc� tem %d segundos para sair da �rea PVP!' % time)
				time = time - 1
				self.players[index] = time
			else:
				if time == 0:
					player.teleport(0, 125, 125)
					Server.send_announcement_all('[Sistema]')
					Server.send_announcement_all('%s movido por ficar na �rea PVP!' % player.get_name())
					del self.players[index]
				else:
					time = time - 1
					self.players[index] = time
		return

	# Movimento
	def on_move(self, index, map, sx, sy, tx, ty):
		player = Player(index)
		if self.open:
			if self.pvp_area(map, tx, ty):
				if not player.is_admin():
					if self.avisados.count(index) > 0 and not index in self.players.keys():
						player.teleport(0, 125, 125)
						Server.send_announcement_all('[Sistema]')
						Server.send_announcement_all('%s movido por entrar na �rea PVP ap�s ser avisado!' % player.get_name())
					if not index in self.players.keys():
						self.avisados.append(index)
						player.message('[Sistema] Saia da �rea de PVP ou sera movido!')
						self.players[index] = 3
			else:
				if index in self.players:
					player.message('[Sistema] Voc� ser� movido se entrar na �rea proibida!')
					del self.players[index] 
		return
	
	# Comando de abrir evento
	def command_abrir(self, player, arguments):
		if not player.is_admin():
			return True
		Server.send_announcement_all('[Sistema] %s bloqueou a �rea de PVP.' % player.get_name())
		Server.send_announcement_all('Quem entrar na �rea proibida')
		Server.send_announcement_all('ser� movido automaticamente!')
		Server.send_message_all('[Sistema] N�o entre na �rea de PVP ou ser� movido!')
		self.players = dict()
		self.avisados = []
		for i in range (Server.player_start, Server.object_max):
			p = Player(i)
			if p.is_admin():
				continue
			if self.pvp_area(p.get_map(), p.get_x(), p.get_y()):
				self.players[p.get_index()] = config.commands.pvp_area_time_notify
				self.avisados.append(p.get_index())
				p.message('[Sistema] ATEN��O: Saia da �rea de PVP ou ser� movido!')
		self.open = True
		return True

	# Comando de fechar evento
	def command_fechar(self, player, arguments):
		if not player.is_admin():
			return True
		Server.send_announcement_all('[Sistema] %s liberou a �rea de PVP.' % player.get_name())
		Server.send_announcement_all('O acesso � �rea est� dispon�vel!')
		self.players = dict()
		self.avisados = []
		self.open = False
		return True

#
# Initialization
#
commands.register(BlockCommand())