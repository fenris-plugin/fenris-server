# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.config import * 

if config.options.non_pk:
	Memory.write_uint32(0x0704E248, 1)
else:
	Memory.write_uint32(0x0704E248, 0)

Memory.write_uint32(0x00459BA2+3, config.options.max_level)
Memory.write_uint32(0x00462A33+2, config.options.max_level)
Memory.write_uint32(0x00496D93+1, config.options.max_level)
Memory.write_uint32(0x0048A2A7+3, config.options.max_level)

Memory.write_uint32(0x00446C81+1, config.options.item_drop_time * 1000)
Memory.write_uint32(0x00446DCA+1, config.options.item_drop_time * 1000)
Memory.write_uint32(0x00446F4F+1, config.options.item_drop_time * 1000)
Memory.write_uint32(0x00447148+1, config.options.item_drop_time * 1000)

Memory.write_uint8(0x0045AC76+3, config.options.disconnect_time + 1)

#Max Zen
Memory.write_uint32(0x0041E40F, config.options.zen_max_inventory) #Max zen inventario

Memory.write_uint32(0x00422DA4, config.options.zen_max_warehouse) #Max Zen bau
Memory.write_uint32(0x00422E9A, config.options.zen_max_warehouse) #Max Zen bau
Memory.write_uint32(0x00422DCF, config.options.zen_max_warehouse) #Max Zen bau

#Potions
Memory.write_uint8(0x00427E35+2, config.options.recovery_apple) #Apple
Memory.write_uint8(0x00427E5E+2, config.options.recovery_smallhp) #Small HP
Memory.write_uint8(0x00428006+2, config.options.recovery_smallmp) #Small MP
Memory.write_uint8(0x00427E87+2, config.options.recovery_mediumhp) #Medium HP
Memory.write_uint8(0x00428045+2, config.options.recovery_mediummp) #Medium MP
Memory.write_uint8(0x00427EB0+2, config.options.recovery_largehp) #Large HP
Memory.write_uint8(0x00428084+2, config.options.recovery_largemp) #Large MP
#Level Up Points
Memory.write_uint8(0x00462B0E, config.options.points_normal) #Pontos Char Normal
Memory.write_uint8(0x00462AF7, config.options.points_mg) #Pontos MG
#Orbs
Memory.write_uint8(0x00457E40, config.options.orb_goblin) #Goblin
Memory.write_uint8(0x00457E71, config.options.orb_golem) #StoneGolem
Memory.write_uint8(0x00457EA2, config.options.orb_assassin) #Assasin
Memory.write_uint8(0x00457ED3, config.options.orb_yeti) #EliteYeti
Memory.write_uint8(0x00457F04, config.options.orb_darkknight) #DarkKnight
Memory.write_uint8(0x00457F35, config.options.orb_bali) #Bali
Memory.write_uint8(0x00457F66, config.options.orb_soldier) #Soldier

#ManaShield
if config.options.manashield_ratio != 0:
	Memory.write_uint8(0x00458794+3, config.options.manashield_ratio) #Porcentagem
else:
	Console.error('manashield_ratio não pode ser 0 (zero).')

if config.options.manashield_dexterity != 0:
	Memory.write_uint32(0x00458779+1, config.options.manashield_dexterity) #Agilidade
else:
	Console.error('manashield_dexterity não pode ser 0 (zero).')
	
if config.options.manashield_energy1 != 0:
	Memory.write_uint32(0x0045878D+1, config.options.manashield_energy1) #Energia
else:
	Console.error('manashield_energy1 não pode ser 0 (zero).')
	
if config.options.manashield_energy2 != 0:
	Memory.write_uint32(0x004587AC+1, config.options.manashield_energy2) #Energia
else:
	Console.error('manashield_energy2 não pode ser 0 (zero).')
	
if config.options.manashield_time != 0:
	Memory.write_uint8(0x004587B3+2, config.options.manashield_time) #Tempo
else:
	Console.error('manashield_time não pode ser 0 (zero).')
	
#GreatForitiute (elf)
if config.options.elfbuff_vitality != 0:
	Memory.write_uint32(0x004583B0+1, config.options.elfbuff_vitality) #Vitalidade
else:
	Console.error('elfbuff_vitality não pode ser 0 (zero).')

if config.options.elfbuff_energy != 0:
	Memory.write_uint32(0x004583C4+1, config.options.elfbuff_energy) #Energia
else:
	Console.error('elfbuff_energy não pode ser 0 (zero).')

if config.options.elfbuff_time != 0:
	Memory.write_uint32(0x004583E2+1, config.options.elfbuff_time) #Tempo
else:
	Console.error('elfbuff_time não pode ser 0 (zero).')

#Rings Transf
Memory.write_uint32(0x00458C98+3, config.options.transf_ring_dragon) #RingDragon
Memory.write_uint32(0x00458CA1+3, config.options.transf_ring_giant) #RingGiant
Memory.write_uint32(0x00458CAA+3, config.options.transf_ring_skeleton) #RingSkeleton
Memory.write_uint32(0x00458CB3+3, config.options.transf_ring_poisonbull) #RingPoisonBull
Memory.write_uint32(0x00458CBC+3, config.options.transf_ring_thunderlich) #RingThunderLich
Memory.write_uint32(0x00458CC5+3, config.options.transf_ring_deathcow) #RingDeathCow
#Jewels Rate
Memory.write_uint8(0x0047A069+3, 100 - config.options.jewel_rate_soul) #Soul Rate
Memory.write_uint8(0x00479F8A+3, config.options.jewel_rate_luck) #Soul Luck
Memory.write_uint8(0x0047A678+3, 100 - config.options.jewel_rate_life) #Life Rate
#Item Drop Rate
Memory.write_uint32(0x00409C25+1, config.options.item_drop_excellent) #ItemExcDropRate
Memory.write_uint32(0x00409DE3+3, config.options.item_drop_excellent_skill) #ItemExcSkillDropRate
Memory.write_uint32(0x00409DEA+3, config.options.item_drop_excellent_luck) #ItemExcLuckDropRate
Memory.write_uint32(0x00409E36+3, config.options.item_drop_skill) #ItemSkillDropRate
Memory.write_uint32(0x00409E3D+3, config.options.item_drop_luck) #ItemLuckDropRate
#Items Prices
Memory.write_uint32(0x00480021, config.options.jewel_price_bless) #JewelOfBless
Memory.write_uint32(0x0048003B, config.options.jewel_price_soul) #JewelOfSoul
Memory.write_uint32(0x00480056, config.options.jewel_price_chaos) #JewelOfChaos
Memory.write_uint32(0x00480071, config.options.jewel_price_life) #JewelOfLife
Memory.write_uint32(0x0048008B, config.options.jewel_price_creation) #JewelOfCreation
Memory.write_uint32(0x004800DB, config.options.jewel_price_dinorant) #Dinorant
Memory.write_uint32(0x004800A6, config.options.jewel_price_fruit) #Fruit
Memory.write_uint32(0x004800C1, config.options.jewel_price_feather) #BlueFeather
#ChaosMachine
Memory.write_uint8(0x0044926D, 100) #Rate +10
Memory.write_uint8(0x0044927C, 100) #Rate +11
Memory.write_uint8(0x0044A27A, 100) #Asa lv 1
Memory.write_uint8(0x0044A286, 100) #Asa lv 2
#BloodCastle
Memory.write_uint8(0x0049B1F8+1, 15) #Id do item
Memory.write_uint8(0x0049B1FA+1, 12) #Index do item
Memory.write_uint8(0x0049B215+1, 0) #Level do item
Memory.write_uint8(0x0049B20D+1, 0) #Durabilidade
Memory.write_uint8(0x0049B211+1, 0) #Skill
Memory.write_uint8(0x0049B20F+1, 0) #Luck
Memory.write_uint8(0x0049B207+1, 0) #Exc
#No-Pk
Memory.write_uint8(0x48C587, 1)
Memory.write_uint8(0x48C784, 1)
Memory.write_uint8(0x48C981, 1)
Memory.write_uint8(0x48CA99, 1)
#Dif level party
Memory.write_uint32(0x41ECCD, 130)
Memory.write_uint32(0x41EDB0, 130)
Memory.write_uint32(0x41EED8, 130)
#Rate Exp Party 2
Memory.write_uint32(0x467E3F, 200)
Memory.write_uint32(0x467E06, 200 + 40)
#Rate Exp Party 3
Memory.write_uint32(0x467E18, 230)
Memory.write_uint32(0x467DE2, 230 + 50)
#Rate Exp Party 4
Memory.write_uint32(0x467E27, 250)
Memory.write_uint32(0x467DF1, 250 + 60)
#Rate Exp Party 5
Memory.write_uint32(0x467E36, 300)
Memory.write_uint32(0x467E00, 300 + 70)
#Potions
if config.fixes.unlimited_potions:
	Memory.write_uint8(0x00427EE8, 0)	
	Memory.write_uint8(0x00428167, 0)
#Immortal pets
if config.fixes.immortal_pets:
	Memory.write_uint8(0x00465B27, 0x90)
	Memory.write_uint8(0x00465B28, 0x90)