# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.config import *

#
# Colunas
#

BONUS_COL_HELM = 0
BONUS_COL_ARMOR= 1
BONUS_COL_PANTS = 2
BONUS_COL_BOOTS = 3
BONUS_COL_GLOVES = 4
BONUS_COL_DEFENSE = 5
BONUS_COL_BLOCK = 6

#
# Checks
#
def has_bonus(player, bonus):
	
	inventory = player.get_inventory()
	
	if bonus[BONUS_COL_HELM] >= 0:
		if player.get_dbclass() != 48: # MG n�o usa helm
			if not inventory.has_item(2):
				return False
			item = inventory.get_item(2)
			if not item.is_excellent() or not item.is_item(7, bonus[BONUS_COL_HELM]):
				return False
	
	if bonus[BONUS_COL_ARMOR] >= 0:
		if not inventory.has_item(3):
			return False
		item = inventory.get_item(3)
		if not item.is_excellent() or not item.is_item(8, bonus[BONUS_COL_ARMOR]):
			return False
			
	if bonus[BONUS_COL_PANTS] >= 0:
		if not inventory.has_item(4):
			return False
		item = inventory.get_item(4)
		if not item.is_excellent() or not item.is_item(9, bonus[BONUS_COL_PANTS]):
			return False
			
	if bonus[BONUS_COL_GLOVES] >= 0:
		if not inventory.has_item(5):
			return False
		item = inventory.get_item(5)
		if not item.is_excellent() or not item.is_item(10, bonus[BONUS_COL_GLOVES]):
			return False
		
	if bonus[BONUS_COL_BOOTS] >= 0:
		if not inventory.has_item(6):
			return False
		item = inventory.get_item(6)
		if not item.is_excellent() or not item.is_item(11, bonus[BONUS_COL_BOOTS]):
			return False

	return True
# 
# Character calculation
#
def bonus_calc(index):
	player = Player(index)
	for bonus in config.data.bonus.sets:
		if has_bonus(player, bonus):
			if float(bonus[BONUS_COL_DEFENSE]) >= 0:
				player.set_defense(int(player.get_defense() * float(bonus[BONUS_COL_DEFENSE])))
			if float(bonus[BONUS_COL_BLOCK]) >= 0:
				player.set_blockrate(int(player.get_blockrate() * float(bonus[BONUS_COL_BLOCK])))
				
#
# Custom character calculation
#
Events.register('player.set', bonus_calc)