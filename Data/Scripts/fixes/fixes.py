# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.config import *

def nop(addr, qnt):
	for i in range(qnt):
		Memory.write_uint8(addr+i, 0x90)
		
def memset(addr, f, qnt):
	for i in range(qnt):
		Memory.write_uint8(addr+i, f)
		
def memcpy(addr, bytes):
	i = 0
	for v in bytes:
		Memory.write_uint8(addr + i, v)
		i = i + 1

if config.fixes.delete_char != 0:
	Memory.write_uint8(0x417781, 0xEB)
	Memory.write_uint8(0x420D2A, 0x90)
	Memory.write_uint8(0x420D2B, 0x90)
	Memory.write_uint8(0x420DEB, 0x90)
	Memory.write_uint8(0x420DEC, 0x90)

if config.fixes.delete_guild != 0:
	Memory.write_uint8(0x420D2A, 0x90)
	Memory.write_uint8(0x420D2B, 0x90)
	Memory.write_uint8(0x420DEB, 0x90)
	Memory.write_uint8(0x420DEC, 0x90)

if config.fixes.item_level_15 != 0:
	Memory.write_uint8(0x45BCB6+2, 15)
	Memory.write_uint8(0x41187F+2, 15)
else:
	Memory.write_uint8(0x45BCB6+2, 11)
	Memory.write_uint8(0x41187F+2, 11)


nop(0x004118E3, 2)
nop(0x00482377, 2)
nop(0x0045BD20, 2)

if config.fixes.item_option_28:
	Memory.write_uint8(0x0047A673+2, 0x07)
else:
	Memory.write_uint8(0x0047A673+2, 0x04)	

if config.fixes.status:

	#Str
	Memory.write_uint8(0x0042856E+1, 0xB7)
	Memory.write_uint8(0x0045C07D+1, 0xB7)
	Memory.write_uint8(0x0045C10C+1, 0xB7)
	Memory.write_uint8(0x0045C19B+1, 0xB7)
	Memory.write_uint8(0x0045C22A+1, 0xB7)
	Memory.write_uint8(0x00452026+1, 0xB7)
	Memory.write_uint8(0x0045204D+1, 0xB7)
	Memory.write_uint8(0x00452075+1, 0xB7)
	Memory.write_uint8(0x0045209C+1, 0xB7)
	Memory.write_uint8(0x004520CF+1, 0xB7)
	Memory.write_uint8(0x004520EA+1, 0xB7)
	Memory.write_uint8(0x00452106+1, 0xB7)
	Memory.write_uint8(0x00452121+1, 0xB7)
	Memory.write_uint8(0x00452152+1, 0xB7)
	Memory.write_uint8(0x0045216D+1, 0xB7)
	Memory.write_uint8(0x00452189+1, 0xB7)
	Memory.write_uint8(0x004521A4+1, 0xB7)
	Memory.write_uint8(0x004521C2+1, 0xB7)
	Memory.write_uint8(0x004521DE+1, 0xB7)
	Memory.write_uint8(0x004521FA+1, 0xB7)
	Memory.write_uint8(0x00452216+1, 0xB7)
	Memory.write_uint8(0x00452579+1, 0xB7)
	Memory.write_uint8(0x00455379+1, 0xB7)
	Memory.write_uint8(0x0045624F+1, 0xB7)
	Memory.write_uint8(0x0045CC73+1, 0xB7)
	Memory.write_uint8(0x0046AE74+1, 0xB7)
	Memory.write_uint8(0x0047D333+1, 0xB7)
	Memory.write_uint8(0x0047D50D+1, 0xB7)
	Memory.write_uint8(0x0047D69D+1, 0xB7)

	#Agi
	Memory.write_uint8(0x004285B8+1, 0xB7)
	Memory.write_uint8(0x0045C099+1, 0xB7)
	Memory.write_uint8(0x0045C128+1, 0xB7)
	Memory.write_uint8(0x0045C1B7+1, 0xB7)
	Memory.write_uint8(0x0045C246+1, 0xB7)
	Memory.write_uint8(0x00451F95+1, 0xB7)
	Memory.write_uint8(0x00451FB0+1, 0xB7)
	Memory.write_uint8(0x00451FCC+1, 0xB7)
	Memory.write_uint8(0x00451FE7+1, 0xB7)
	Memory.write_uint8(0x0045201C+1, 0xB7)
	Memory.write_uint8(0x00452043+1, 0xB7)
	Memory.write_uint8(0x0045206B+1, 0xB7)
	Memory.write_uint8(0x00452092+1, 0xB7)
	Memory.write_uint8(0x00452583+1, 0xB7)
	Memory.write_uint8(0x004525D5+1, 0xB7)
	Memory.write_uint8(0x004525F0+1, 0xB7)
	Memory.write_uint8(0x00452630+1, 0xB7)
	Memory.write_uint8(0x0045264B+1, 0xB7)
	Memory.write_uint8(0x00452668+1, 0xB7)
	Memory.write_uint8(0x00452683+1, 0xB7)
	Memory.write_uint8(0x00452997+1, 0xB7)
	Memory.write_uint8(0x004529B5+1, 0xB7)
	Memory.write_uint8(0x00452CA8+1, 0xB7)
	Memory.write_uint8(0x00452CD5+1, 0xB7)
	Memory.write_uint8(0x00452CF2+1, 0xB7)
	Memory.write_uint8(0x00455365+1, 0xB7)
	Memory.write_uint8(0x00458771+1, 0xB7)
	Memory.write_uint8(0x0045CC7D+1, 0xB7)
	Memory.write_uint8(0x0046AE92+1, 0xB7)
	Memory.write_uint8(0x0047D33D+1, 0xB7)
	Memory.write_uint8(0x0047D517+1, 0xB7)
	Memory.write_uint8(0x0047D6A7+1, 0xB7)

	#Vit
	Memory.write_uint8(0x0045C0B7+1, 0xB7) 
	Memory.write_uint8(0x0045C146+1, 0xB7) 
	Memory.write_uint8(0x0045C1D5+1, 0xB7) 
	Memory.write_uint8(0x0045C264+1, 0xB7) 
	Memory.write_uint8(0x004583A8+1, 0xB7) 
	Memory.write_uint8(0x0045CC89+1, 0xB7) 
	Memory.write_uint8(0x0045CD8E+1, 0xB7) 
	Memory.write_uint8(0x0047D349+1, 0xB7)
	Memory.write_uint8(0x0047D523+1, 0xB7) 
	Memory.write_uint8(0x0047D6B3+1, 0xB7) 
	Memory.write_uint8(0x0047DF19+1, 0xB7) 
	Memory.write_uint8(0x0047DF3C+1, 0xB7) 
	Memory.write_uint8(0x0047DF5F+1, 0xB7) 
	Memory.write_uint8(0x0047DF82+1, 0xB7)

	#Ene
	Memory.write_uint8(0x0045C0D5+1, 0xB7)
	Memory.write_uint8(0x0045C164+1, 0xB7)
	Memory.write_uint8(0x0045C1F3+1, 0xB7)
	Memory.write_uint8(0x0045C282+1, 0xB7)
	Memory.write_uint8(0x004524C2+1, 0xB7)
	Memory.write_uint8(0x004524DD+1, 0xB7)
	Memory.write_uint8(0x00455B36+1, 0xB7)
	Memory.write_uint8(0x00455B60+1, 0xB7)
	Memory.write_uint8(0x00455BB5+1, 0xB7)
	Memory.write_uint8(0x00455BDF+1, 0xB7)
	Memory.write_uint8(0x00455C31+1, 0xB7)
	Memory.write_uint8(0x00455C5B+1, 0xB7)
	Memory.write_uint8(0x00455CA2+1, 0xB7)
	Memory.write_uint8(0x00455CCC+1, 0xB7)
	Memory.write_uint8(0x00455CF9+1, 0xB7)
	Memory.write_uint8(0x00455D23+1, 0xB7)
	Memory.write_uint8(0x00455D63+1, 0xB7)
	Memory.write_uint8(0x004583BC+1, 0xB7)
	Memory.write_uint8(0x004583DA+1, 0xB7)
	Memory.write_uint8(0x00458785+1, 0xB7)
	Memory.write_uint8(0x004587A4+1, 0xB7) 
	Memory.write_uint8(0x004588CD+1, 0xB7)
	Memory.write_uint8(0x00458A5E+1, 0xB7)
	Memory.write_uint8(0x00458EC8+1, 0xB7)
	Memory.write_uint8(0x0045CC95+1, 0xB7)
	Memory.write_uint8(0x0045CE48+1, 0xB7)
	Memory.write_uint8(0x004790F5+1, 0xB7)
	Memory.write_uint8(0x00479511+1, 0xB7)
	Memory.write_uint8(0x0047D355+1, 0xB7)
	Memory.write_uint8(0x0047D52F+1, 0xB7)
	Memory.write_uint8(0x0047D6BF+1, 0xB7)	

if config.fixes.potions_255 != 0:
	nop(0x004119FE, 10)
	nop(0x0045BE0E, 7)

if config.fixes.pk_events:
	Memory.write_uint8(0x42AD19, 0xEB)
	Memory.write_uint8(0x429455, 0xEB)

if config.fixes.pk_shop != 0:
	Memory.write_uint8(0x41B797, 0xEB)

if config.fixes.pk_pvp != 0:
	Memory.write_uint8(0x45509C, 0xEB)
	Memory.write_uint8(0x455077, 0xEB)

if config.fixes.pk_bug != 0:
	Memory.write_uint8(0x45508A, 0x90)
	Memory.write_uint8(0x45508B, 0x90)
	Memory.write_uint8(0x45508C, 0x90)
	Memory.write_uint8(0x45508D, 0x90)
	Memory.write_uint8(0x45508E, 0x90)
	Memory.write_uint8(0x4550AF, 0x90)
	Memory.write_uint8(0x4550B0, 0x90)
	Memory.write_uint8(0x4550B1, 0x90)
	Memory.write_uint8(0x4550B2, 0x90)
	Memory.write_uint8(0x4550B3, 0x90)
	Memory.write_uint8(0x4642A4, 0x9C)
	Memory.write_uint8(0x4642B0, 0x9C)
	Memory.write_uint8(0x4643C0, 0x9C)
	Memory.write_uint8(0x4643CC, 0x9C)

if config.fixes.pk_limit != 0:
	memset(0x465585, 0x90, 15)

if config.fixes.standby != 0:
	nop(0x00462BD7, 5)					
	nop(0x0049700A, 5)

#Autoridade do nick Webzen
if config.fixes.webzen != 0:
	Memory.write_uint8(0x45D33A, 0x01)
	Memory.write_uint8(0x45D37B, 0x01)
	Memory.write_uint8(0x45D3E3, 0x01)

#New Check X Y Map
if config.fixes.map != 0:
	memcpy(0x461022, [0xE9,0xA2,0x00,0x00,0x00,0x90])

if config.fixes.serial != 0:
	Memory.write_uint8(0x00414239, 0xEB)

if config.fixes.hack != 0:
	Memory.write_uint8(0x004172E0, 0xC3)

if config.fixes.iocp != 0:
	Memory.write_uint8(0x40165E, 0xC3)
	Memory.write_uint8(0x40165F, 0x90)
	Memory.write_uint8(0x401660, 0x90)
	Memory.write_uint8(0x401661, 0x90)
	Memory.write_uint8(0x401652, 0x90)

if config.fixes.invalid_socket != 0:
	Memory.write_uint8(0x440EF8, 0x90)
	Memory.write_uint8(0x440EF9, 0x90)
	Memory.write_uint8(0x440F23, 0xEB)

if config.fixes.hacktool != 0:
	Memory.write_uint8(0x0040289A, 0xC3) 
	Memory.write_uint8(0x0040289B, 0x90) 
	Memory.write_uint8(0x0040289C, 0x90) 
	Memory.write_uint8(0x0040289D, 0x90) 
	Memory.write_uint8(0x0040289E, 0x90)

if config.fixes.memory != 0:
	Memory.write_uint8(0x0048BB6C, 0xEB)

if config.fixes.speed != 0:
	Memory.write_uint8(0x0041536C, 0xEB)
	Memory.write_uint8(0x00415379, 0xEB)

	Memory.write_uint8(0x0042570E, 0xEB)
	Memory.write_uint8(0x00457574, 0xEB)

	nop(0x41532B, 6)

	memset(0x004024E6, 0xC3, 1)
	memset(0x004256C4, 0x90, 6)

if config.fixes.levels != 0:

	Memory.write_uint8(0x48107D, 0x7E)

	# Fix Event Enter DevilSquare 1000
	Memory.write_uint32(0x00429843+3, 0x3E8)
	Memory.write_uint32(0x0042981B+3, 0x3E8)

	# Fix Event Enter  BloodCastle Level 1000
	Memory.write_uint8(0x00494A76, 0x00)
	Memory.write_uint8(0x004949DA, 0x00)

if config.fixes.l2 != 0:
	Memory.write_uint8(0x44029B, 0x90)
	Memory.write_uint8(0x44029C, 0x90)
	Memory.write_uint8(0x44029D, 0x90)
	Memory.write_uint8(0x44029E, 0x90)
	Memory.write_uint8(0x44029F, 0x90)
	Memory.write_uint8(0x4402A0, 0x90)

if config.fixes.gqcs != 0:
	Memory.write_uint8(0x0043EEAE, 0xEB)

if config.fixes.zen_party != 0:
	memcpy(0x4E3437, [0x8B, 0x4D, 0x0C, 0x33, 0xC0, 0x8A, 0x41, 0x64, 0x83, 0xF8, 0x02, 0x74, 0x05, 0x83, 0xF8, 0x03, 0x75, 0x20, 0x8B, 0x91, 0xB4, 0x00, 0x00, 0x00, 0x89, 0x55, 0xF8, 0xDB, 0x45, 0xF8, 0xD8, 0x0D, 0xC8, 0x18, 0x6E, 0x00, 0xE8, 0x1C, 0x55, 0x0B, 0x00, 0x8B, 0x4D, 0x0C, 0x89, 0x81, 0xB4, 0x00, 0x00, 0x00, 0x5F, 0x5E, 0x5B, 0x8B, 0xE5, 0x5D, 0xC3])

if config.fixes.protocol != 0:
	nop(0x426EA1, 27)

if config.options.disable_checksum != 0:
	nop(0x415579, 32)

if config.options.infinity_arrow != 0:
	nop(0x00454CB3, 45)
	nop(0x00454D82, 45)

Memory.write_uint8(0x43E632+1, 0x7F)
Memory.write_uint8(0x43E09E+1, 0x7F)