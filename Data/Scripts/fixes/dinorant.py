# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.config import *

# 
# Character calculation
#

def fix_dinorant(index):
	player = Player(index)
	inventory = player.get_inventory()
	if not inventory.has_item(8): 
		return
	item = inventory.get_item(8)
	if item.get_section() == 13 and item.get_index() == 3: 
		player.set_defense(int(player.get_defense() * config.fixes.dinorant_defense_rate))
		player.set_blockrate(int(player.get_blockrate() * config.fixes.dinorant_block_rate))

#
# Custom character calculation
#

Events.register('player.set', fix_dinorant)
