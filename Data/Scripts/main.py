# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils import *
from utils.database import *
from utils.config import *
from fixes import *
from commands import *
from systems import *
from customs import *

# 
# Main
#
def main():
	database.set('muonline', config.database.muonline)
	conn = database.get('muonline')
	if conn == False:
		Server.close_error("Dados do SQL invalidos.\nPor favor configure a conexao no arquivo config/database.ini")
	else:
		with conn.cursor() as cur:
			cur.execute("SELECT 1")
			if cur.fetchone()[0] != 1:
				Console.error("[Python] Falha na conexao com o banco de dados!")
				Server.close_error("[Python] Falha na conexao com o banco de dados!")
				return
		for bs in glob.glob("..\\Data\\Scripts\\bootstrap\\*.py"):
			execfile(bs)	
		Console.success('[Python] Conectado ao banco de dados!')

	Console.debug("")
	Console.debug("  Servidor: %s (%d) @ %s:%d" % (Server.server_name, Server.server_code, Server.server_address, Server.server_port))
	Console.debug("Executavel: \"%s\"" % (Server.server_executable))
	Console.debug(" Diretorio: \"%s\"" % (Server.server_directory))
	Console.debug("   Comando: %s" % (Server.server_command))
	Console.debug("")
