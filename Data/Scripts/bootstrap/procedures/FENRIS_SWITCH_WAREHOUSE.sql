SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FENRIS_SWITCH_WAREHOUSE](@account VARCHAR(10), @new_id INT)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON

	DECLARE @current_id INT	
	DECLARE @new_items VARBINARY(1200)
	DECLARE @new_money INT
	DECLARE @new_dbid TINYINT
	DECLARE @maxwarehouses INT
	
	DECLARE @old_items VARBINARY(1200)
	DECLARE @old_money INT
	DECLARE @old_dbid TINYINT
	
	DECLARE @success INT
	SET @success = 1
		
	BEGIN TRY

		BEGIN TRANSACTION
		
		-- Se o cara nunca tiver aberto o ba�, criar o ba� padr�o
		IF NOT EXISTS (SELECT * FROM warehouse WHERE AccountID = @account) 
		BEGIN
			INSERT INTO warehouse (AccountID, WarehouseId, Items, Money, EndUseDate, DbVersion, pw) VALUES (@account, 0, NULL, 0, NULL, 1, NULL)	
		END

		-- Seleciona as configura��es atuais (ba� atual / quantidade m�xima)
		SELECT @current_id = WarehouseId, @maxwarehouses = WarehouseCount FROM warehouse WHERE AccountID = @account
		
		-- Verifica se a pessoa possui os ba�s necess�rios
		IF (@new_id > @maxwarehouses OR @new_id < 0) 
		BEGIN
			SET @success = 2
		END
		ELSE
		BEGIN
			-- Garante exist�ncia das entradas utilizadas (novo ba�)
			IF NOT EXISTS (SELECT * FROM warehouses WHERE AccountID = @account AND Id = @new_id)
			BEGIN
				INSERT INTO warehouses (AccountID, Id, Items, Money, DbVersion) VALUES (@account, @new_id, NULL, 0, 1)
			END

			-- Garante exist�ncia das entradas utilizadas (ba� atual)
			IF NOT EXISTS (SELECT * FROM warehouses WHERE AccountID = @account AND Id = @current_id)
			BEGIN
				INSERT INTO warehouses (AccountID, Id, Items, Money, DbVersion) VALUES (@account, @current_id, NULL, 0, 1)
			END

			-- Realiza a troca se necess�rio
			IF (@current_id <> @new_id)
			BEGIN
				SELECT @old_items = Items, @old_money = Money, @old_dbid = DbVersion FROM warehouse WHERE AccountID = @account
				SELECT @new_items = Items, @new_money = Money, @new_dbid = DbVersion FROM warehouses WHERE AccountID = @account AND Id = @new_id
				UPDATE warehouse SET WarehouseId = @new_id, Items = @new_items, Money = @new_money, DbVersion = @new_dbid WHERE AccountID = @account
				UPDATE warehouses SET Items = @old_items, Money = @old_money, DbVersion = @old_dbid WHERE AccountID = @account AND Id = @current_id		
			END
		END
		
		COMMIT TRANSACTION
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SET @success = 0
	END CATCH
	
	SET XACT_ABORT OFF
	SET NOCOUNT OFF

	SELECT @success as 'Success'
	
END
