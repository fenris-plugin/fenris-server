#
# Imports
#

from wolfulus import *
from utils.database import *
from utils.config import *
import os, glob

#
# Columns
#

def bootstrap_columns():
	# Duel
	database.create_column('Character', config.columns.duel_wins, 'INT NOT NULL DEFAULT 0')
	database.create_column('Character', config.columns.duel_losses, 'INT NOT NULL DEFAULT 0')

	# Resets
	database.create_column('Character', config.columns.resets, 'INT NOT NULL DEFAULT 0')
	database.create_column('Character', config.columns.mresets, 'INT NOT NULL DEFAULT 0')

	# PK/HERO
	database.create_column('Character', config.columns.hero_count, 'INT NOT NULL DEFAULT 0')
	database.create_column('Character', config.columns.pk_count, 'INT NOT NULL DEFAULT 0')

	# Account
	database.create_column('MEMB_INFO', config.columns.vip, 'INT NOT NULL DEFAULT 0')
	database.create_column('MEMB_INFO', config.columns.cash, 'INT NOT NULL DEFAULT 0')

	# Warehouse
	database.create_column('warehouse', 'WarehouseId', 'INT NOT NULL DEFAULT 0')
	database.create_column('warehouse', 'WarehouseCount', 'INT NOT NULL DEFAULT 0')

	# Ban 
	database.create_column('MEMB_INFO', config.columns.ban_timer, 'DATETIME NULL')
	database.create_column('Character', config.columns.banchar_timer, 'DATETIME NULL')

	# Prizes
	for prize in config.data.prizes.list:
		database.create_column(prize[3], prize[4], 'INT NOT NULL DEFAULT 0')

#
# Procedures
#

def bootstrap_procedures():
	db = database.get('muonline')
	for proc in glob.glob("..\\Data\\Scripts\\bootstrap\\procedures\\*.sql"):
		try:
			query = ''
			cursor = db.cursor()
			with open (proc, "r") as procfile:
				for line in procfile:
					if line.strip() == 'GO':
						Console.debug('QUERY: %s' % query)
						cursor.execute(query)
						query = ''
					else:
						query = query + line
				if query.strip() != '':
					Console.debug('QUERY: %s' % query)
					cursor.execute(query)
				cursor.commit()
				cursor.close()
		except:
			Console.debug("")
			Console.debug("PROCEDURE ALREADY EXISTS?")
			Console.debug("")
			pass

#
# Tables
#

def bootstrap_tables():
	db = database.get('muonline')
	for proc in glob.glob("..\\Data\\Scripts\\bootstrap\\tables\\*.sql"):
		try:
			query = ''
			cursor = db.cursor()
			with open (proc, "r") as procfile:
				for line in procfile:
					if line.strip() == 'GO':
						Console.debug('QUERY: %s' % query)
						cursor.execute(query)
						query = ''
					else:
						query = query + line
				if query.strip() != '':
					Console.debug('QUERY: %s' % query)
					cursor.execute(query)
				cursor.commit()
				cursor.close()
		except:
			Console.debug("")
			Console.debug("TABLE ALREADY EXISTS?")
			Console.debug("")
			pass

#
# Execute
#

bootstrap_tables()
bootstrap_columns()
bootstrap_procedures()