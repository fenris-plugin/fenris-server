SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GameServerList] (
	[Code] [int] NOT NULL,
	[Process] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,

	[Address] [varchar](20) NOT NULL,
	[Port] [int] NOT NULL,

	[Load] [int] NOT NULL DEFAULT 0,
	[Users] [int] NOT NULL DEFAULT 0,
	[MaxUsers] [int] NOT NULL DEFAULT 0,

	[Executable] [varchar](250) NULL,
	[Directory] [varchar](250) NULL,
	[Command] [varchar](250) NULL,

	[Visible] [int] NOT NULL DEFAULT 1,
	[Heartbeat] [datetime] NULL

  CONSTRAINT [PK_FENRISSERVERS] PRIMARY KEY CLUSTERED
  (
  	[Code] ASC
  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
