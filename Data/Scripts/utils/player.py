# -*- coding: UTF-8 -*-

#
# Imports
#

from wolfulus import *
from inventory import * 
from item import * 
from database import *
from timer import * 
from config import *
from native import * 
from guild import *

import struct
import textwrap

#
# Player class
#
class Player(object):
	
	# Constants
	PLAYER_EMPTY = 0
	PLAYER_CONNECTED = 1
	PLAYER_LOGGED = 2	
	PLAYER_PLAYING = 3

	# Data cache
	caches = dict()

	# Constructor
	def __init__(self, index):
		self.index = index
		self.offset = Server.object_address(index)
	
	# Gets the player index
	def get_index(self):
		return self.index
	# Sets the player index
	def set_index(self, val):
		self.index = val
		self.offset = Server.object_address(val)

	# Gets a variable address
	def get_address(self, variable):
		addr = self.offset + variable
		return addr

	# Gets the player name
	def get_name(self):
		return Memory.read_string(self.get_address(0x6A), 10)

	# Gets the player account
	def get_account(self):
		return Memory.read_string(self.get_address(0x5F), 10)

	# Gets the class
	def get_class(self):
		return Memory.read_uint8(self.get_address(0x83))
	def set_class(self, val):
		return Memory.write_uint8(self.get_address(0x83), val)

	# Gets the class (database)
	def get_dbclass(self):
		return Memory.read_uint8(self.get_address(0x84))
	def set_dbclass(self, val):
		return Memory.write_uint8(self.get_address(0x84), val)

	# Gets the class changeup
	def get_changeup(self):
		return Memory.read_uint8(self.get_address(0x85))
	def set_changeup(self, val):
		return Memory.write_uint8(self.get_address(0x85), val)

	# Gets the class changeup
	def get_quest(self, index):
		if index < 0 or index >= 50:
			return False
		return Memory.read_uint8(self.get_address(0xCAD + index))
	def set_quest(self, index, val):		
		if index < 0 or index >= 50:
			return False
		return Memory.write_uint8(self.get_address(0xCAD + index), val)
		
		
	# Strength
	def get_strength(self):
		return Memory.read_uint16(self.get_address(0x98))
	def set_strength(self, val):
		return Memory.write_uint16(self.get_address(0x98), val)

	# Dexterity
	def get_dexterity(self):
		return Memory.read_uint16(self.get_address(0x9A))
	def set_dexterity(self, val):
		return Memory.write_uint16(self.get_address(0x9A), val)

	# Vitality
	def get_vitality(self):
		return Memory.read_uint16(self.get_address(0x9C))
	def set_vitality(self, val):
		return Memory.write_uint16(self.get_address(0x9C), val)

	# Energy
	def get_energy(self):
		return Memory.read_uint16(self.get_address(0x9E))
	def set_energy(self, val):
		return Memory.write_uint16(self.get_address(0x9E), val)

	# Points
	def get_points(self):
		return Memory.read_uint32(self.get_address(0x88))
	def set_points(self, val):
		Memory.write_uint32(self.get_address(0x88), val)

	# AddMana
	def get_addmana(self):
		return Memory.read_uint16(self.get_address(0xDC))
	def set_addmana(self, val):
		Memory.write_uint16(self.get_address(0xDC), val)

	# AddLife
	def get_addlife(self):
		return Memory.read_uint16(self.get_address(0xDA))
	def set_addlife(self, val):
		Memory.write_uint16(self.get_address(0xDA), val)

	# AddBP
	def get_addbp(self):
		return Memory.read_uint32(self.get_address(0xC0))
	def set_addbp(self, val):
		Memory.write_uint32(self.get_address(0xC0), val)

	# AddBP
	def get_maxbp(self):
		return Memory.read_uint32(self.get_address(0xBC))
	def set_maxbp(self, val):
		Memory.write_uint32(self.get_address(0xBC), val)

	# BP
	def get_bp(self):
		return Memory.read_uint32(self.get_address(0xB8))
	def set_bp(self, val):
		Memory.write_uint32(self.get_address(0xB8), val)

	# Gets the attack damage min
	def get_attackdmgmin(self):
		return Memory.read_uint32(self.get_address(0x288))
	def set_attackdmgmin(self, value):
		return Memory.write_uint32(self.get_address(0x288), value)

	# Gets the attack damage max
	def get_attackdmgmax(self):
		return Memory.read_uint32(self.get_address(0x28C))
	def set_attackdmgmax(self, value):
		return Memory.write_uint32(self.get_address(0x28C), value)

	# Gets the attack damage max
	def get_attackdmgleft(self):
		return Memory.read_uint32(self.get_address(0x298))
	def set_attackdmgleft(self, value):
		return Memory.write_uint32(self.get_address(0x298), value)

	# Gets the attack damage max
	def get_attackdmgright(self):
		return Memory.read_uint32(self.get_address(0x29C))
	def set_attackdmgright(self, value):
		return Memory.write_uint32(self.get_address(0x29C), value)

	# Gets the attack damage max
	def get_attackdmgleftmax(self):
		return Memory.read_uint32(self.get_address(0x2A0))
	def set_attackdmgleftmax(self, value):
		return Memory.write_uint32(self.get_address(0x2A0), value)

	# Gets the attack damage max
	def get_attackdmgleftmin(self):
		return Memory.read_uint32(self.get_address(0x2A4))
	def set_attackdmgleftmin(self, value):
		return Memory.write_uint32(self.get_address(0x2A4), value)

	# Gets the attack damage max
	def get_attackdmgrightmax(self):
		return Memory.read_uint32(self.get_address(0x2A8))
	def set_attackdmgrightmax(self, value):
		return Memory.write_uint32(self.get_address(0x2A8), value)

	# Gets the attack damage max
	def get_attackdmgrightmin(self):
		return Memory.read_uint32(self.get_address(0x2AC))
	def set_attackdmgrightmin(self, value):
		return Memory.write_uint32(self.get_address(0x2AC), value)

	# Rate
	def get_attackrate(self):
		return Memory.read_uint32(self.get_address(0x2B0))
	def set_attackrate(self, val):
		Memory.write_uint32(self.get_address(0x2B0), val)

	# Speed
	def get_attackspeed(self):
		return Memory.read_uint32(self.get_address(0x2B4))
	def set_attackspeed(self, val):
		Memory.write_uint32(self.get_address(0x2B4), val)

	# Life
	def get_life(self):
		return Memory.read_float(self.get_address(0xA0))
	def set_life(self, val):
		Memory.write_float(self.get_address(0xA0), float(val))

	# MaxLife
	def get_maxlife(self):
		return Memory.read_float(self.get_address(0xA4))
	def set_maxlife(self, val):
		Memory.write_float(self.get_address(0xA4), float(val))

	# Mana
	def get_mana(self):
		return Memory.read_float(self.get_address(0xB0))
	def set_mana(self, val):
		Memory.write_float(self.get_address(0xB0), float(val))

	# MaxMana
	def get_maxmana(self):
		return Memory.read_float(self.get_address(0xB4))
	def set_maxmana(self, val):
		Memory.write_float(self.get_address(0xB4), float(val))

	# FillLife
	def get_filllife(self):
		return Memory.read_float(self.get_address(0xA8))
	def set_filllife(self, val):
		Memory.write_float(self.get_address(0xA8), float(val))

	# FillLifeMax
	def get_filllifemax(self):
		return Memory.read_float(self.get_address(0xAC))
	def set_filllifemax(self, val):
		Memory.write_float(self.get_address(0xAC), float(val))

	# Gets the player authority
	def get_authority(self):
		return Memory.read_uint32(self.get_address(0x170))
		
	# Gets the player authority code
	def get_authority_code(self):
		return Memory.read_uint32(self.get_address(0x174))
		
	# Defense
	def get_defense(self):
		return Memory.read_uint32(self.get_address(0x2BC))
	def set_defense(self, val):
		Memory.write_uint32(self.get_address(0x2BC), val)
				
	# Magic Defense
	def get_magicdefense(self):
		return Memory.read_uint32(self.get_address(0x2C0))
	def set_magicdefense(self, val):
		Memory.write_uint32(self.get_address(0x2C0), val)
				
	# Blocking rate
	def get_blockrate(self):
		return Memory.read_uint32(self.get_address(0x2C4))
	def set_blockrate(self, val):
		Memory.write_uint32(self.get_address(0x2C4), val)
		
	# Gets the player skin
	def get_skin(self):
		return Memory.read_int32(self.get_address(0x27C))
				
	# Gets the player skin
	def set_skin(self, skin):
		ret = Memory.write_int32(self.get_address(0x27C), skin)	
		self.refresh()
		return ret

	# Gets the player close type
	def get_closetype(self):
		return Memory.read_uint8(self.get_address(0x0B))
	def set_closetype(self, value):
		return Memory.write_uint8(self.get_address(0x0B), value)

	# Gets the player close count
	def get_closecount(self):
		return Memory.read_uint8(self.get_address(0x0A))
	def set_closecount(self, value):
		return Memory.write_uint8(self.get_address(0x0A), value)
		
	# Gets the player level
	def get_level(self):
		return Memory.read_uint16(self.get_address(0x86))
	def set_level(self, val):
		return Memory.write_uint16(self.get_address(0x86), val)

	# Gets the player exp
	def get_exp(self):
		return Memory.read_uint32(self.get_address(0x8C))
	def set_exp(self, val):
		return Memory.write_uint32(self.get_address(0x8C), val)

	# Gets the player map number
	def get_map(self):
		return Memory.read_uint8(self.get_address(0xD9))
	def set_map(self, value):
		return Memory.write_uint8(self.get_address(0xD9), value)
		
	# Gets the player position x
	def get_x(self):
		return Memory.read_uint16(self.get_address(0xD4))
	def set_x(self, value):
		return Memory.write_uint16(self.get_address(0xD4), value)
		
	# Gets the player position y
	def get_y(self):
		return Memory.read_uint16(self.get_address(0xD6))
	def set_y(self, value):
		return Memory.write_uint16(self.get_address(0xD6), value)

	# Gets the player pk level
	def get_pklevel(self):
		return Memory.read_uint8(self.get_address(0xCD))
	def set_pklevel(self, value):
		ret = Memory.write_uint8(self.get_address(0xCD), value)
		pkt = struct.pack('BBBBBBB', 0xC1, 0x07, 0xF3, 0x08, (self.get_index() >> 8) & 0xFF, self.get_index() & 0xFF, value & 0xFF)
		for p in self.get_near_players():
			Server.send_packet(p, pkt, len(pkt))
		self.packet(pkt)
		return ret

	# Gets the player pk count
	def get_pkcount(self):
		return Memory.read_uint8(self.get_address(0xCC))
	def set_pkcount(self, value):
		return Memory.write_uint8(self.get_address(0xCC), value)

	# Gets the player pk time
	def get_pktime(self):
		return Memory.read_uint32(self.get_address(0xD0))
	def set_pktime(self, value):
		return Memory.write_uint32(self.get_address(0xD0), value)

	# Gets the inventory
	def get_inventory(self):
		return Inventory(Memory.read_uint32(self.get_address(0xC24)))

	# Money
	def get_money(self):
		return Memory.read_uint32(self.get_address(0x94))
	def set_money(self, value):
		ret = Memory.write_uint32(self.get_address(0x94), value)
		self.packet(struct.pack('BBBBBBBB', 0xC3, 0x08, 0x22, 0xFE, (value >> 24) & 0xFF, (value >> 16) & 0xFF, (value >> 8) & 0xFF, value & 0xFF))
		return ret

	# Gets a list of all the near players
	def get_near_players(self):
		objs = []
		base = self.get_address(0x32C)
		for i in range(75):
			vp = base + (0x0C * i)
			state = Memory.read_uint8(vp)
			number = Memory.read_uint16(vp + 2)
			type = Memory.read_uint8(vp + 4)
			if (state == 1 or state == 2) and type == 1: # user
				if number >= 0 and number < Server.object_max: # range
					objs.append(number)
		return objs

	# Gets a list of all the near monsters
	def get_near_monsters(self):
		objs = []
		base = self.get_address(0x32C)
		for i in range(75):
			vp = base + (0x0C * i)
			state = Memory.read_uint8(vp)
			number = Memory.read_uint16(vp + 2)
			type = Memory.read_uint8(vp + 4)
			if (state == 1 or state == 2) and (type == 2 or type == 3): # monster
				if number >= 0 and number < Server.object_max: # range
					objs.append(number)
		return objs

	# Gets a list of all the near monsters
	def get_near_objects(self):
		objs = []
		base = self.get_address(0x32C)
		for i in range(75):
			vp = base + (0x0C * i)
			state = Memory.read_uint8(vp)
			number = Memory.read_uint16(vp + 2)
			type = Memory.read_uint8(vp + 4)
			if (state == 1 or state == 2): 
				if number >= 0 and number < Server.object_max: # range
					objs.append(number)
		return objs

	# Has guild
	def has_guild(self):
		return self.get_guildnumber() != 0 and len(self.get_guildname()) > 0

	# Check guild master
	def is_guild_master(self):
		if self.has_guild():
			guild = self.get_guild()
			return guild.get_master_name() == self.get_name()
		return False


	# Guild Name
	def get_guildname(self):
		return Memory.read_string(self.get_address(0x26C), 10)
		
	# Guild Number
	def get_guildnumber(self):
		return Memory.read_uint32(self.get_address(0x264))

	# Guild structure
	def get_guild(self):
		return Guild(Memory.read_uint32(self.get_address(0x268)))

	# Checks if player is admin
	def is_live(self):
		return Memory.read_uint8(self.get_address(0x5D)) != 0

	# Checks if player is admin
	def is_admin(self):
		return self.get_authority() & 2 == 2

	# Check if connected
	def is_connected(self):
		return Memory.read_int32(self.get_address(0x04)) >= self.PLAYER_CONNECTED
	
	# Check if logged
	def is_loggedin(self):
		return Memory.read_int32(self.get_address(0x04)) >= self.PLAYER_LOGGED
		
	# Check if connected
	def is_playing(self):
		return Memory.read_int32(self.get_address(0x04)) >= self.PLAYER_PLAYING and self.get_name() != ''
			

	#
	# Interface
	#

	def get_interface(self):
		return Memory.read_uint8(self.get_address(0xC1E))

	def set_interface(self, value):
		Memory.write_uint8(self.get_address(0xC1E), value)

	def get_interface_use(self):
		return (self.get_interface() & 0x01) != 0
		
	def set_interface_use(self, value):
		if value:
			self.set_interface(self.get_interface() | 0x01)
		else:
			self.set_interface(self.get_interface() & (~0x01))

	def get_interface_state(self):
		return (self.get_interface() >> 2) & 0x03
		
	def set_interface_state(self, value):
		self.set_interface((self.get_interface() & (~0x0C)) | ((value & 0x03) << 2))

	def get_interface_type(self):
		return (self.get_interface() >> 4) & 0x0F
		
	def set_interface_type(self, value):
		self.set_interface((self.get_interface() & (~0xF0)) | ((value & 0x0F) << 4))

	# Warehouse Count
	def get_warehouse_count(self):
		return Memory.read_uint8(self.get_address(0xC64))
	def set_warehouse_count(self, value):
		return Memory.write_uint8(self.get_address(0xC64), value)

	# Shop Time
	def get_shop_time(self):
		return Memory.read_uint8(self.get_address(0x1B4))
	def set_shop_time(self, value):
		return Memory.write_uint8(self.get_address(0x1B4), value)

	# Target Shop Number
	def get_target_shop_number(self):
		return Memory.read_uint16(self.get_address(0x282))
	def set_target_shop_number(self, value):
		return Memory.write_uint16(self.get_address(0x282), value)

	# Teleports the character
	def teleport(self, map, x, y):
		Server.teleport(self.index, map, x, y)
		return
		
	# Disconnects the current player
	def disconnect(self):		
		Server.disconnect(self.index)
		return
		
	# Sends a message
	def announce(self, msg): 
		msg = textwrap.wrap(msg, 55)
		msg = [x.strip(', ') for x in msg]
		for m in msg:
			Server.send_announcement(self.index, m)
		return
		
	# Sends a message
	def message(self, msg):
		msg = textwrap.wrap(msg, 55)
		msg = [x.strip(', ') for x in msg]
		for m in msg:
			Server.send_message(self.index, m)
		return

	# Sends a message
	def message_whisper(self, name, msg):
		msg = textwrap.wrap(msg, 55)
		msg = [x.strip(', ') for x in msg]
		for m in msg:
			Server.send_message_whisper(self.index, name, m)
		return

	# Sends a message
	def message_guild(self, name, msg):
		msg = textwrap.wrap(msg, 55)
		msg = [x.strip(', ') for x in msg]
		for m in msg:
			Server.send_message_guild(self.index, name, m)
		return

		# Sends a message
	def message_party(self, name, msg):
		msg = textwrap.wrap(msg, 55)
		msg = [x.strip(', ') for x in msg]
		for m in msg:
			Server.send_message_whisper(self.index, name, m)
		return

	# Refreshes the player on the screen
	def refresh(self):
		Server.refresh(self.get_index(), self.get_map(), self.get_x(), self.get_y())
		return

	# Refreshes the player on the screen
	def refill(self):
		Server.refill(self.get_index())
		return

	# Sends a packet
	def packet(self, packet):
		Server.send_packet(self.get_index(), packet, len(packet))
		return 

	# Character selection
	def select_character(self):
		self.set_closecount(1)
		self.set_closetype(1)

	def delete_item(self, index):
		if (index >= 0 and index < 76):
			gameserver.gObjInventoryDeleteItem(self.get_index(), index)
			gameserver.GCInventoryItemDeleteSend(self.get_index(), index, 0)
			return True
		else:
			return False

	def attack(self, damage):
		gameserver.gObjAttack(self.get_address(0x00), self.get_address(0x00))
		return

	def kill(self):
		self.set_life(-1.0)
		gameserver.gObjLifeCheck(self.get_address(0x00), self.get_index(), int(1), int(0), int(0), int(0), int(0))

	#
	# Database access
	#

	def is_vip(self):
		vip = self.get_cache('vip')
		if vip != None:
			return vip
		else:
			vip = self.get_db_value('MEMB_INFO', config.columns.vip, None, memb___id = self.get_account())
			self.set_cache('vip', vip)
			if vip == None:
				vip = False
		return vip
	
	#
	# Resets
	#
	def get_resets(self):
		val = self.get_cache('resets')
		if val == None:
			if not isinstance(config.columns.resets, list):
				val = self.get_db_value('Character', config.columns.resets, None, Name = self.get_name())
			else:
				val = self.get_db_value('Character', config.columns.resets[0], None, Name = self.get_name())
			self.set_cache('resets', val)
		return val
			
	def set_resets(self, value):
		self.set_cache('resets', value)
		if not isinstance(config.columns.resets, list):
			self.set_db_value('Character', config.columns.resets, value, Name = self.get_name())
		else:
			self.set_db_value('Character', config.columns.resets[0], value, Name = self.get_name())

	def inc_resets(self):
		if not isinstance(config.columns.resets, list):
			self.inc_db_value('Character', config.columns.resets, Name = self.get_name())
		else:
			for column in config.columns.resets:
				self.inc_db_value('Character', column, Name = self.get_name())
		self.clear_cache('resets')

	#
	# Warehouses
	#
	def get_current_warehouse_id(self):
		return self.get_db_value('warehouse', 'WarehouseId', None, AccountID = self.get_account())

	def get_extra_warehouse_count(self):
		return self.get_db_value('warehouse', 'WarehouseCount', None, AccountID = self.get_account())

	#
	# Master Resets
	#
	def get_mresets(self):
		val = self.get_cache('mresets')
		if val == None:
			if not isinstance(config.columns.resets, list):
				val = self.get_db_value('Character', config.columns.mresets, None, Name = self.get_name())
			else:
				val = self.get_db_value('Character', config.columns.mresets[0], None, Name = self.get_name())			
			self.set_cache('mresets', val)
		return val
			
	def set_mresets(self, value):
		self.set_cache('mresets', value)
		if not isinstance(config.columns.resets, list):
			self.set_db_value('Character', config.columns.mresets, value, Name = self.get_name())
		else:
			self.set_db_value('Character', config.columns.mresets[0], value, Name = self.get_name())

	def inc_mresets(self):
		if not isinstance(config.columns.mresets, list):
			self.inc_db_value('Character', config.columns.mresets, Name = self.get_name())
		else:
			for column in config.columns.mresets:
				self.inc_db_value('Character', column, Name = self.get_name())
		self.clear_cache('mresets')
		
	#
	# Duels
	# 
	def inc_duel_wins(self):
		if not isinstance(config.columns.duel_wins, list):
			self.inc_db_value('Character', config.columns.duel_wins, Name = self.get_name())
		else:
			for column in config.columns.duel_wins:
				self.inc_db_value('Character', column, Name = self.get_name())
		self.clear_cache('duel_wins')

	def inc_duel_losses(self):
		if not isinstance(config.columns.duel_losses, list):
			self.inc_db_value('Character', config.columns.duel_losses, Name = self.get_name())
		else:
			for column in config.columns.duel_losses:
				self.inc_db_value('Character', column, Name = self.get_name())
		self.clear_cache('duel_losses')

	#
	# Where build
	#
	def db_where(self, **kargs):
		sql = []
		args = []
		if len(kargs) > 0:
			for key in kargs:
				sql.append('%s = ?' % key)
				args.append(kargs[key])
			sql = "WHERE %s" % ', '.join(sql)
		else:
			sql = ""
		return { 'query': sql, 'args': args }

	#
	# Get value
	#
	def get_db_value(self, table, column, default = None, **kargs):
		where = self.db_where(**kargs)
		db = database.get('muonline')
		cursor = db.cursor()
		cursor.execute("SELECT %s FROM %s %s" % (column, table, where['query']), where['args'])
		row = cursor.fetchone()
		if not row:
			value = default
		else:
			value = row[column.lower()]

		cursor.close()
		return value

	#
	# Set value
	#
	def set_db_value(self, table, column, value, **kargs):
		where = self.db_where(**kargs)
		db = database.get('muonline')
		cursor = db.cursor()
		cursor.execute("UPDATE %s SET %s = ? %s" % (table, column, where['query']), [value] + where['args'])
		cursor.close()
		db.commit()
	
	#
	# Increase value 
	#
	def inc_db_value(self, table, column, **kargs):
		self.inc_db_value_by(table, column, 1, **kargs)

	def inc_db_value_by(self, table, column, count, **kargs):
		where = self.db_where(**kargs)
		db = database.get('muonline')
		cursor = db.cursor()
		cursor.execute("UPDATE %s SET %s = %s + ? %s" % (table, column, column, where['query']), [count] + where['args'])
		cursor.close()
		db.commit()

	#
	# Decrease value 
	#
	def dec_db_value(self, table, column, **kargs):
		self.dec_db_value_by(table, column, 1, **kargs)

	def dec_db_value_by(self, table, column, count, **kargs):
		where = self.db_where(**kargs)
		db = database.get('muonline')
		cursor = db.cursor()
		cursor.execute("UPDATE %s SET %s = %s - ? %s" % (table, column, column, where['query']), [count] + where['args'])
		cursor.close()
		db.commit()

	#
	# Variable caching (performance)
	#

	def get_cache(self, var, default = None):
		var = var.lower()
		idx = self.get_index()
		name = self.get_name().lower()
		account = self.get_account().lower()
		if not idx in Player.caches:
			Player.caches[idx] = dict()
			Player.caches[idx]['__name__'] = name
			Player.caches[idx]['__account__'] = account
		else:
			if Player.caches[idx]['__name__'] != name or Player.caches[idx]['__account__'] != account:
				del Player.caches[idx]
 				Player.caches[idx] = dict()
				Player.caches[idx]['__name__'] = name
				Player.caches[idx]['__account__'] = account

		if not var in Player.caches[idx]:
			return default
		else: 
			return Player.caches[idx][var]
	
	def set_cache(self, var, value):
		var = var.lower()
		idx = self.get_index()
		name = self.get_name().lower()
		account = self.get_account().lower()
		if not idx in Player.caches:
			Player.caches[idx] = dict()
			Player.caches[idx]['__name__'] = name
			Player.caches[idx]['__account__'] = account
		else:
			if Player.caches[idx]['__name__'] != name or Player.caches[idx]['__account__'] != account:
				del Player.caches[idx]
				Player.caches[idx] = dict()
				Player.caches[idx]['__name__'] = name
				Player.caches[idx]['__account__'] = account
		Player.caches[idx][var] = value

	def clear_cache(self, var):
		var = var.lower()
		idx = self.get_index()
		name = self.get_name().lower()
		account = self.get_account().lower()
		if not idx in Player.caches:
			Player.caches[idx] = dict()
			Player.caches[idx]['__name__'] = name
			Player.caches[idx]['__account__'] = account
		else:
			if Player.caches[idx]['__name__'] != name or Player.caches[idx]['__account__'] != account:
				del Player.caches[idx]
				Player.caches[idx] = dict()
				Player.caches[idx]['__name__'] = name
				Player.caches[idx]['__account__'] = account
		if var in Player.caches[idx]:
			del Player.caches[idx][var]
