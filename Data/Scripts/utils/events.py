
#
# Event class
#
class Event:
	
	def __init__(self):
		self._handlers = set()

	def subscribe(self, handler):
		self._handlers.add(handler)
		return self

	def unsubscribe(self, handler):
		try:
			self._handlers.remove(handler)
		except:
			raise ValueError("Handler is not handling this event, so cannot unhandle it.")
			return self

	def emit(self, *args, **kargs):
		for handler in self._handlers:
			handler(*args, **kargs)

	def get_handler_count(self):
		return len(self._handlers)

	__iadd__ = subscribe
	__isub__ = unsubscribe
	__call__ = emit
	__len__  = get_handler_count

#
# Event Emitter
#
class EventEmitter:

	def __init__(sekf):
		self._events = dict()

	def on(self, name, callback):
		if not name in self._events:
			self._events[name] = list()
		self._events[name].append(callback)

	def emit(self, name, *args, **kwargs):
		if name in self._events:
			for callback in self._events[name]:
				callback(self, *args, **kwargs)

	def clear(self, name):
		if name in self._events:
			del self._events[name]
