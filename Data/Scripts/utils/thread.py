# -*- coding: UTF-8 -*-

from wolfulus import *
from utils.player import *
from utils.monster import *
from utils.config import *
from utils.timer import *

import threading

#
# Threaded function
#
def threaded(fn):
    def run(*k, **kw):
        t = threading.Thread(target=fn, args=k, kwargs=kw)
        t.start()
        return t # <-- this is new!
    return run