#
# Imports
#

from wolfulus import *

#
# Guild class
#
class Guild(object):

	MAX_USER_GUILD = 35

	# Constructor
	def __init__(self, address):
		self.offset = address
		
	# Gets a variable address
	def get_address(self, variable):
		addr = self.offset + variable
		return addr
		
	def get_number(self):
		return Memory.read_uint32(self.get_address(0x00))
	
	def get_name(self):
		return Memory.read_string(self.get_address(0x04), 8)

	def get_mark(self):
		mark = []
		for i in range(32):
			mark.append(Memory.read_uint8(self.get_address(0x0D + i)))
		return mark

	def get_count(self):
		return Memory.read_uint8(self.get_address(0x2D))

	def get_totalcount(self):
		return Memory.read_uint8(self.get_address(0x2E))

	def get_member_name(self, index):
		if self.is_active(index):
			return Memory.read_string(self.get_address(0x2F + (11 * index)), 10)

	def get_member_index(self, index):
		if self.is_active(index):
			return Memory.read_int16(self.get_address(0x1B0 + (2 * index)))

	def is_active(self, index):
		if index >= Guild.MAX_USER_GUILD:
			return False
		return Memory.read_uint8(self.get_address(0x1F6 + index)) != 0

	def get_member_server(self, index):
		if self.is_active(index):
			server = int(Memory.read_uint8(self.get_address(0x219 + index)))
			if server == 0xFF:
				return -1
			else:
				return server
		return -1

	def is_member_online(self, index):
		if not self.is_active(index):
			return False
		if self.get_member_server(index) != -1:
			return True
		return False

	def is_member_online_here(self, index):
		if not self.is_active(index):
			return False
		server = self.get_member_server(index)
		if server != -1 and server == Server.server_code:
			return True
		return False

	def get_online_here(self):
		members = []
		for i in range(Guild.MAX_USER_GUILD):
			if self.is_active(i):
				if self.get_member_server(i) == Server.server_code:
					members.append(i)
		return members

	def get_online(self):
		members = []
		for i in range(Guild.MAX_USER_GUILD):
			if self.is_active(i):
				if self.get_member_server(i) != -1:
					members.append(i)
		return members

	def get_master_name(self):
		return self.get_member_name(0)
	def get_master_index(self):
		return self.get_member_index(0)
	def get_master_server(self):
		return self.get_member_server(0)
	def is_master_online(self):
		return self.is_member_online(0)
	def is_master_online_here(self):
		return self.is_member_online_here(0)

	def get_wardeclarestate(self):
		return Memory.read_uint8(self.get_address(0x290))
	def get_warstate(self):
		return Memory.read_uint8(self.get_address(0x291))
	def get_wartype(self):
		return Memory.read_uint8(self.get_address(0x292))
	def get_groundindex(self):
		return Memory.read_uint8(self.get_address(0x293))
	def get_soccer_team(self):
		return Memory.read_uint8(self.get_address(0x294))
	def get_score(self):
		return Memory.read_uint32(self.get_address(0x295))
	
	def get_notice(self):
		return Memory.read_string(self.get_address(0x29C), 60)