from wolfulus import *
from utils.config import * 
import time
import sys

def profiler(f):
	def f_timer(*args, **kwargs):
		if not config.options.profile:
			return f(*args, **kwargs)
		else:
			error = False
			try:
				start = time.clock()
				Console.warn('PROFILER START: %s()' % (f.__name__))
				result = f(*args, **kwargs)
				return result
			except:
				error = True
				ex = sys.exc_info()
				raise ex[1], None, ex[2]
			finally:
				end = time.clock()
				if error:
					Console.error('PROFILER END: %s() took %.6f (WITH ERROR)' % (f.__name__, end - start))
				else:
					Console.warn('PROFILER END: %s() took %.6f' % (f.__name__, end - start))

	return f_timer
