# -*- coding: UTF-8 -*-

#
# Imports
#

from wolfulus import *
from utils.profiler import *

import math
import sys
import traceback

#
# Callback with data
#
class TimerCallback(object):

	# Constructor
	def __init__(self, callback, *args, **kwargs):
		self.callback = callback
		self.args = args
		self.kwargs = kwargs
		return

	# Call wrapper
	def __call__(self):
		self.callback(*self.args, **self.kwargs)
		return

#
# Callback with data
#
class TimerCounter(object):

	# Constructor
	def __init__(self, callback, count, *args, **kwargs):
		self.callback = callback
		self.args = args
		self.kwargs = kwargs
		self.count = count
		return

	# Call wrapper
	def __call__(self):
		self.callback(self.count, *self.args, **self.kwargs)
		self.count = self.count - 1
		return

#
# Timer class
#
class Timer(object):

	# Constructor
	def __init__(self, callback, step, count, trace):
		self.callback = callback
		self.current = 0
		self.last = 0
		self.count = count
		self.step = step
		self.trace = trace
		return
#
# Timer manager
#
class TimerManager(object):

	# Constructor
	def __init__(self):
		Events.register('timer.tick', self.tick)
		self.timers = dict()
		self.id = 0
		return

	# Resets all timers
	def reset(self):
		self.timers = dict()
		return

	# Clears a timer
	def clear(self, index):
		if self.timers.has_key(index):
			del self.timers[index]
		return

	# Timeout timers
	def timeout(self, callback, step):

		if step == 0:
			Console.error("")
			Console.error("Intervalo de timer zerado (timeout):")
			for trace in timer.trace:
				Console.error(trace)
			Console.error("")
			Console.error("")
			return None
					
		id = self.id + 1
		self.id = id
		self.timers[id] = Timer(callback, step, -1, traceback.format_stack())

		return id

	# Interval timers
	def interval(self, callback, step):

		if step == 0:
			Console.error("")
			Console.error("Intervalo de timer zerado (interval):")
			for trace in timer.trace:
				Console.error(trace)
			Console.error("")
			Console.error("")
			return None

		id = self.id + 1
		self.id = id
		self.timers[id] = Timer(callback, step, 0, traceback.format_stack())

		return id

	# Repeat timers
	def repeat(self, callback, step, count):

		if step == 0:
			Console.error("")
			Console.error("Intervalo de timer zerado (repeat):")
			for trace in timer.trace:
				Console.error(trace)
			Console.error("")
			Console.error("")
			return False

		id = self.id + 1
		self.id = id
		self.timers[id] = Timer(callback, step, 1 + count, traceback.format_stack())

		return id

	def update(self):
		self.tick(0)

	# Tick event
	#@profiler
	def tick(self, ms):
		for index in self.timers.keys():
			timer = self.timers[index]
			timer.current = timer.current + ms
			if timer.current - timer.last >= timer.step:

				if timer.step == 0:
					Console.error("")
					Console.error("Intervalo de timer zerado. Verificar em:")
					for trace in timer.trace:
						Console.error(trace)
					Console.error("")
					Console.error("")

				count = math.floor((timer.current - timer.last) / timer.step)
				remainder = (timer.current - timer.last) % timer.step
				timer.last = timer.current - remainder
				self.timers[index] = timer
				if self.timers[index].count < 0: # Timeout
					try:
						self.timers[index].callback()
					except Exception as e:
						Console.error(traceback.format_exc())
					self.clear(index)
				elif self.timers[index].count == 0: # Interval
					while count > 0 and self.timers.has_key(index):
						try:
							self.timers[index].callback()
						except Exception as e:
							Console.error(traceback.format_exc())
						count = count - 1
				elif self.timers[index].count > 1: # Repeats
					while count > 0 and self.timers.has_key(index):
						try:
							self.timers[index].callback()
						except Exception as e:
							Console.error(traceback.format_exc())
						self.timers[index].count = self.timers[index].count - 1
						if self.timers[index].count == 1:
							self.clear(index)
							break
						count = count - 1
				else:
					self.clear(index)
		return

#
# Timer 
#
timer = TimerManager()