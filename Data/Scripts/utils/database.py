# -*- coding: UTF-8 -*-

#
# Imports
#
import odbc
from wolfulus import *

#
# Database instances
#
class Database(object):

	#
	# Constructor
	#
	def __init__(self):
		self.instances = {}
	#
	# Configures a database
	#
	def set(self, name, str):
		self.instances[name] = str
			
	#
	# Gets the database instance
	#
	def get(self, name):
		if name in self.instances:
			settings = self.instances[name]
			cnx = odbc.connect(settings, timeout=5)
			return cnx
		return False

	#
	# Functions
	#
	def execute(self, query):
		_db = database.get('muonline')
		_cursor = _db.cursor()
		_cursor.execute(query)
		_db.commit()
		_cursor.close()
		
	def create_column(self, table, column, definition):
		if not isinstance(column, list):
			self.execute("IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '%s' and COLUMN_NAME = '%s') ALTER TABLE %s ADD %s %s" % (table, column, table, column, definition))
		else:
			for col in column:
				self.execute("IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '%s' and COLUMN_NAME = '%s') ALTER TABLE %s ADD %s %s" % (table, col, table, col, definition))
				
database = Database()

