# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from ctypes import *
from ctypes.wintypes import *

#
# Native GameServer calls
#
class Native(object):

	#
	# Constructor
	#
	def __init__(self):
		self.gObjInventoryDeleteItem = self.cdecl(0x004695F0, None, (c_int, c_int))
		self.GCInventoryItemDeleteSend = self.cdecl(0x00429140, None, (c_int, c_int, BYTE))
		self.gObjLifeCheck = self.cdecl(0x004684A0, None, (c_void_p, c_void_p, c_int, c_int, c_int, BYTE, c_int))
		self.gObjDel = self.cdecl(0x00460640, None, (c_int))
		self.GDGetWarehouseList = self.cdecl(0x004019C9, None, (c_int, c_void_p))
		self.gObjMakePreviewCharSet = self.cdecl(0x0046FE30, None, (c_int))
		self.GCSendQuestPrize = self.cdecl(0x0042AA70, None, (c_int, BYTE, BYTE))
		return

	#
	# Standard calls
	#
	def stdcall(self, address, restype, args):
		prototype = None
		if type(args) is tuple:
			prototype = WINFUNCTYPE(restype, *args)
		else:
			prototype = WINFUNCTYPE(restype, args)
			
		function = prototype(address)
		return function
		
	#
	# C calls
	#
	def cdecl(self, address, restype, args):
		prototype = None
		if type(args) is tuple:
			prototype = CFUNCTYPE(restype, *args)
		else:
			prototype = CFUNCTYPE(restype, args)
			
		function = prototype(address)
		return function

gameserver = Native()