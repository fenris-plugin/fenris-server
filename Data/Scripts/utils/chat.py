# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.config import * 

#
# Handler class
#
class ChatHandler(object):

	# Constructor
	def __init__(self):
		self.handlers = []
		Events.register('player.message', self.chat)
		return
		
	# Register a command
	def register(self, handler):
		self.handlers.append(handler)
		return
		 
	# Process the incoming text into commands
	def process(self, player, text):

		command = text.split(' ')
		if len(command) <= 0:
			return False
		arguments = filter(None, command[1:])
		command = command[0].lower()

		for move in config.data.moves.list:
			if ('/%s' % move[0]) == command:
				self.move(player, move)
				return True
			elif len(arguments) == 1:
				if command == '/move' and arguments[0] == move[0]:
					self.move(player, move)
					return True

		for handler in self.handlers:
			if handler.execute(player, text, command, arguments):
				return True

		return False
		
	#
	# Move command
	#
	def move(self, player, move):

		# //nome			mapa		x		y		level		zen			vip 	resets
		if player.get_level() < int(move[4]):
			player.message('[Sistema] Voc� precisa de level %d para entrar em %s' % (int(move[4]), move[0]))
			return 

		if player.get_money() < int(move[5]):
			player.message('[Sistema] Voc� precisa de %d zen para entrar em %s' % (int(move[5]), move[0]))
			return 

		if int(move[6]) != 0:
			if not player.is_vip():
				player.message('[Sistema] Voc� precisa ser VIP para entrar em %s' % move[0])
				return 

		if len(move) == 8:
			if int(move[7]) > 0:
				if player.get_resets() < int(move[7]):
					player.message('[Sistema] Voc� precisa ter pelomenos %d resets para entrar em %s' % (int(move[7]), move[0]))
					return
			elif int(move[7]) < 0:
				if player.get_resets() > -int(move[7]):
					player.message('[Sistema] Voc� precisa ter menos de %d resets para entrar em %s' % (-int(move[7]), move[0]))
					return

		if (int(move[1]) == 10):
			inv = player.get_inventory()
			if not inv.has_item(7):
				if not inv.has_item(8):
					player.message('[Sistema] Voc� n�o pode entrar em %s sem asa ou dinorant.' % move[0])
					return
				if not inv.get_item(8).is_item(13, 3):
					player.message('[Sistema] Voc� n�o pode entrar em %s sem asa ou dinorant.' % move[0])
					return

		if (int(move[1]) == 7):
			inv = player.get_inventory()
			if inv.has_item(8):
				mount = inv.get_item(8)
				if mount.is_item(13, 2) or mount.is_item(13, 3):
					player.message('[Sistema] Voc� n�o pode entrar em %s com montaria.' % move[0])
					return

		player.set_money(player.get_money() - int(move[5]))
		player.teleport(int(move[1]), int(move[2]), int(move[3]))

		player.message('[Sistema] Voc� foi teleportado para %s' % move[0])

	#
	# Chat handler
	#
	#@profiler
	def chat(self, index, text):
		player = Player(index)
		if text.startswith('$'):
			if player.is_admin():
				msg = text[1:]
				if len(msg) > 0:
					if msg[0] == '$':
						msg = msg[1:]
						if len(msg) > 0:
							Server.broadcast_announcement('[%s]' % player.get_name())
							Server.broadcast_announcement(msg)
					else:
						Server.send_announcement_all('[%s]' % player.get_name())
						Server.send_announcement_all(msg)
				return True
			return True
		elif text.startswith('!'):
			if player.is_admin():
				msg = text[1:]
				if len(msg) > 0:
					if msg[0] == '!':
						msg = msg[1:]
						if len(msg) > 0:
							Server.broadcast_announcement(msg)
						return True

		return self.process(player, text)
#
# Command class
#		
class Command(object):
	
	# Constructor
	def __init__(self):	
		self.dispatchers = dict()
		self.dispatchers_text = dict()
		self.initialize()

	# Startup
	def initialize():
		pass
		
	# Registers a new dispatcher to this command
	def register(self, prefix, dispatcher):
		self.dispatchers[prefix.lower()] = dispatcher
		return
		
	# Registers a new dispatcher to this command
	def register_text(self, prefix, dispatcher):
		self.dispatchers_text[prefix.lower()] = dispatcher
		return

	# Executes a dispatcher
	def execute(self, player, text, command, arguments):
		if command in self.dispatchers:
			dispatcher = self.dispatchers[command]
			ret = dispatcher(player, arguments)
			if ret is None:
				return True
			return ret
		elif command in self.dispatchers_text:
			dispatcher = self.dispatchers_text[command]
			ret = dispatcher(player, text[len(command)+1:])
			if ret is None:
				return True
			return ret
		return False

#
# Command handler instance
#
commands = ChatHandler()