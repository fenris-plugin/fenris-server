# -*- coding: UTF-8 -*-

#
# Imports
#

from wolfulus import *
from ConfigParser import ConfigParser
from ConfigParser import RawConfigParser
import glob, os
import traceback

#
# Configuration entry
#
class FenrisConfigData(object):

	#
	# Constructor
	#
	def __init__(self, section):
		self.section = section
		pass

	# 
	#
	#
	def __getattr__(self, attr):
		Console.error("")
		Console.error("")
		Console.error("Erro de configuracao!")
		Console.error("Arquivo nao possui as secoes necessarias: %s" % attr)
		Console.error("Verifique em:")
		Console.error("    %s" % attr)
		Console.error("")
		Console.error("")
		return []

#
# Configuration entry
#
class FenrisConfig(object):

	#
	# Constructor
	#
	def __init__(self, filename, section):
		self.filename = filename
		self.section = section

	# 
	#
	#
	def __getattr__(self, attr):

		Console.error("")
		Console.error("")
		Console.error("Erro de configuracao!")
		Console.error("Uma configuracao parece estar faltando.")

		fname = self.filename
		if self.section == 'data':
			fname = os.path.join(os.getcwd(), '..\\Data\\Scripts\\config\\%s.dat' % attr)

		Console.error("Por favor verifique o arquivo:")
		Console.error("")
		Console.error("    %s" % fname)
		Console.error("")

		if self.section != 'data':
			Console.error("e configure o valor necessario:")
			Console.error("")
			Console.warn("[%s]" % self.section)
			Console.warn("%s = ????" % attr)
			Console.error("")
			Console.error("")

		"""
		if fname != False and os.path.isfile(fname) and self.section != 'data':			
			config = RawConfigParser()
			config.read(self.filename)
			try:
				config.add_section(self.section)
			except:
				pass
			config.set(self.section, attr, '????')
			with open(self.filename, 'wb') as cfgfile:
				config.write(cfgfile)
		"""

		return 0
#
# Configuration class
#
class FenrisConfigs(object):

	#
	# Constructor
	#
	def __init__(self):

		self.data = FenrisConfig(False, 'data')

		self.directory = os.path.abspath(os.getcwd()) + "\\..\\Data\\Scripts\\config\\"

		for filename in glob.glob(self.directory + "*.ini"):

			config = ConfigParser()
			config.read(filename)

			for section in config.sections():
				if section.lower() == 'data':
					Console.error('')
					Console.error('Erro de configuracao!')
					Console.error("Nome 'data' nao pode ser usado como nome de secao.")
					Console.error('')
					continue
				section = section.lower()
				values = FenrisConfig(filename, section)
				for item in config.items(section):
					name = item[0].lower()
					try:
						value = item[1]
						if (item[1] != ""):
							value = eval(item[1])
						setattr(values, name, value)
					except:
						Console.error('')
						Console.error("Falha ao ler configura��o [%s] %s:" % (section, name))
						Console.error("Verifique o arquivo: ")
						Console.error("    %s" % (filename))
						Console.error('Este valor pode ter vindo em uma alguma atualizacao')
						Console.error('e precisa ser configurado adequadamente.')
						Console.error('')
						Console.error(traceback.format_exc())
				setattr(self, section, values)

		dats = glob.glob(self.directory + "*.dat")
		for dat in dats:
			datname = os.path.basename(dat)
			datname = os.path.splitext(datname)[0].lower()
			content = self.parse_dat(dat)

			obj = FenrisConfigData(dat)
			for section in content:
				setattr(obj, section, content[section])

			setattr(self.data, datname, obj)

	#
	#
	#
	def __getattr__(self, attr):

		Console.error("")
		Console.error("")
		Console.error("Erro de configuracao!")
		Console.error("Uma secao com nome '%s' esta faltando nas configuracoes." % attr)
		Console.error("Por favor verifique se nenhuma configuracao foi removida")
		Console.error("ou se voce esqueceu de adicionar em algum update")
		Console.error("")
		Console.error("")

		"""
		filename = "..\\Data\\Scripts\\config\\%s." % attr
		if not os.path.isfile(filename + "ini") and not os.path.isfile(filename + "dat"):

			cfg = RawConfigParser()
			cfg.add_section(attr)

			with open(filename + "ini", 'wb') as cfgfile:
				cfg.write(cfgfile)

			Console.error("Para facilitar, criamos um arquivo chamado %s.ini na" % attr)
			Console.error("pasta config para que voce possa configurar o que falta.")
			Console.error("")
		"""
		
		newcfg = FenrisConfig(filename + 'ini', attr)
		setattr(self, attr, newcfg)
		return newcfg

	# 
	# Get configuration
	#
	def get(self, section, name):
		data = getattr(self, section)
		if data:
			return getattr(data, name)

		raise Exception("Missing configuration: %s.%s" % (section, name))

	#
	# Parser do arquivo de configura��es
	#
	def parse_dat(self, dat):

		token = ""
		section = False
		sectionName = ""

		content = {}
		rows = []
		rowLength = 0

		lines = [line.rstrip('\r\n') for line in open(dat)]

		for line in lines:
			
			line = line.strip()
			if line == '' or line[:2] == '//':
				continue

			token = ""
			quote = False
			tokens = []

			for c in [c for c in line]:
				if c == ' ' or c == '\t':
					if token == '':
						continue
					else:
						if quote == False:
							tokens.append(token)
							quote = False
							token = ""
						else:
							token = token + c
				else:
					if c == '"':
						token = token + c
						if quote == False:
							quote = True
							continue
						else: 
							quote = False
							tokens.append(token)
							token = ""
					else:
						token = token + c

			if token != '':
				tokens.append(token)

			if sectionName == "":
				if len(tokens) != 1:
					raise Exception('Formato do arquivo %s inv�lido. Section est� faltando.' % dat)
				else:
					sectionName = tokens[0]
					content[sectionName] = []
			else:
				if len(tokens) == 1:
					if tokens[0].lower() == 'end':
						sectionName = ""
				else:
					for i in range(len(tokens)):
						if tokens[i][:2] == '//':
							tokens = tokens[:i]
							break 

					size = len(content[sectionName])
					if size > 0 and len(tokens) != len(content[sectionName][0]):
						raise Exception('Formato do arquivo %s inv�lido. Quantidade diferente de colunas no mesmo bloco.' % dat)
					else:
						try:
							tokens = [eval(t) for t in tokens]
						except:
							raise Exception(('Valor inv�lido no arquivo %s\n' + t) % dat)
						content[sectionName].append(tokens)

		return content

config = FenrisConfigs()

