# -*- coding: UTF-8 -*-

#
# Não referenciar esse arquivo... 
# O mesmo está presente só para utilização como referência às funções do servidor
#

class Console:

	@staticmethod
	def write(text):
		pass

	@staticmethod
	def log(text):
		pass
		
	@staticmethod
	def success(text):
		pass

	@staticmethod
	def warn(text):
		pass

	@staticmethod
	def warning(text):
		pass

	@staticmethod
	def error(text):
		pass

	@staticmethod
	def err(text):
		pass

	@staticmethod
	def dbg(text):
		pass

	@staticmethod
	def debug(text):
		pass

class Events:

	@staticmethod
	def register(name, handler):
		pass

	@staticmethod
	def clear():
		pass
		
class Memory:

	@staticmethod
	def read_string(addr, len):
		pass

	@staticmethod
	def write_string(addr, value):
		pass

	@staticmethod
	def read_bool(addr):
		pass

	@staticmethod
	def write_bool(addr, value):
		pass

	@staticmethod
	def read_int8(addr):
		pass

	@staticmethod
	def write_int8(addr, value):
		pass

	@staticmethod
	def read_uint8(addr):
		pass

	@staticmethod
	def write_uint8(addr, value):
		pass

	@staticmethod
	def read_int16(addr):
		pass

	@staticmethod
	def write_int16(addr, value):
		pass

	@staticmethod
	def read_uint16(addr):
		pass

	@staticmethod
	def write_uint16(addr, value):
		pass

	@staticmethod
	def read_int32(addr):
		pass

	@staticmethod
	def write_int32(addr, value):
		pass

	@staticmethod
	def read_uint32(addr):
		pass

	@staticmethod
	def write_uint32(addr, value):
		pass

	@staticmethod
	def read_int64(addr):
		pass

	@staticmethod
	def write_int64(addr, value):
		pass

	@staticmethod
	def read_uint64(addr):
		pass

	@staticmethod
	def write_uint64(addr, value):
		pass

	@staticmethod
	def read_float(addr):
		pass

	@staticmethod
	def write_float(addr, value):
		pass

	@staticmethod
	def read_double(addr):
		pass

	@staticmethod
	def write_double(addr, value):
		pass



class Map:
	def __init__(self, number):
		pass

	def spawn(self, mon, x, y, dir):
		pass

	def drop(self, type, index, level, dur, x, y, skill, luck, option, excellent, serial, owner):
		pass

	def drop_serial(self, type, index, level, dur, x, y, skill, luck, option, excellent, owner):
		pass

	def drop_money(self, money, x, y):
		pass


class Server:

	object_min = 0
	object_max = 0
	object_size = 0
	player_start = 0
	player_count = 0
	object_base = 0

	@staticmethod
	def object_address(index):
		pass

	@staticmethod
	def send_message(index, message):
		pass
	@staticmethod
	def send_message_all(message):
		pass
		
	@staticmethod
	def send_message_party(index, who, message):
		pass
	@staticmethod
	def send_message_party_all(who, message):
		pass

	@staticmethod
	def send_message_whisper(index, who, message):
		pass
	@staticmethod
	def send_message_whisper_all(who, message):
		pass

	@staticmethod
	def send_message_guild(index, who, message):
		pass
	@staticmethod
	def send_message_guild_all(who, message):
		pass

	@staticmethod
	def send_announcement(index, message):
		pass
	@staticmethod
	def send_announcement_all(message):
		pass

	@staticmethod
	def find_by_name(name):
		pass
	@staticmethod
	def find_by_account(account):
		pass

	@staticmethod
	def disconnect(index):
		pass

	@staticmethod
	def teleport(index, map, x, y):
		pass

	@staticmethod
	def log(message):
		pass

	@staticmethod
	def update_money(index):
		pass

	@staticmethod
	def reload_monsters():
		pass

	@staticmethod
	def apply_skill(source, target, skill):
		pass

	@staticmethod
	def get_distance(source, target):
		pass

	@staticmethod
	def send_packet(index, buffer, size):
		pass

	@staticmethod
	def refresh(index, map, x, y):
		pass

	@staticmethod
	def recalc(index):
		pass

	@staticmethod
	def npc_message(player, npc, message):
		pass

	@staticmethod
	def refill(index):
		pass

	@staticmethod
	def close_error(message):
		pass