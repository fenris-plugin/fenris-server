from wolfulus import *
from utils.player import *

#
# Monster
#
class Monster(Player):

	#
	# Constructor
	#
	def __init__(self, id):
		Player.__init__(self, id)

	#
	# Gets the class
	#
	def get_class(self):
		return Memory.read_uint8(self.get_address(0x83))

	#
	# Gets the movespeed
	#
	def get_mtx(self):
		return Memory.read_uint8(self.get_address(0xEE))
	def set_mtx(self, value):
		return Memory.write_uint8(self.get_address(0xEE), value)

	#
	# Gets the movespeed
	#
	def get_mty(self):
		return Memory.read_uint8(self.get_address(0xF0))
	def set_mty(self, value):
		return Memory.write_uint8(self.get_address(0xF0), value)

	#
	# Gets the movespeed
	#
	def get_movespeed(self):
		return Memory.read_uint16(self.get_address(0x2C8))
	def set_movespeed(self, value):
		return Memory.write_uint16(self.get_address(0x2C8), value)

	#
	# Gets the moverange
	#
	def get_moverange(self):
		return Memory.read_uint16(self.get_address(0x2CA))
	def set_moverange(self, value):
		return Memory.write_uint16(self.get_address(0x2CA), value)

	#
	# Gets the attackrange
	#
	def get_attackrange(self):
		return Memory.read_uint16(self.get_address(0x2CC))
	def set_attackrange(self, value):
		return Memory.write_uint16(self.get_address(0x2CC), value)

	#
	# Gets the attack type
	#
	def get_attacktype(self):
		return Memory.read_uint16(self.get_address(0x2CE))
	def set_attacktype(self, value):
		return Memory.write_uint16(self.get_address(0x2CE), value)

	#
	# Gets the view range
	#
	def get_viewrange(self):
		return Memory.read_uint16(self.get_address(0x2D0))
	def set_viewrange(self, value):
		return Memory.write_uint16(self.get_address(0x2D0), value)

	#
	# Gets the 
	#
	def get_attribute(self):
		return Memory.read_uint16(self.get_address(0x2D2))
	def set_attribute(self, value):
		return Memory.write_uint16(self.get_address(0x2D2), value)

	#
	# Drop Money
	#
	def drop_money(self, money):
		Map(self.get_map()).drop_money(money, self.get_x(), self.get_y())
		
	#
	# Drop
	#
	def drop(self, section, index, level, dur, skill, luck, option, excellent, player = None):
		if player is None:
			Map(self.get_map()).drop_serial(section, index, level, dur, self.get_x(), self.get_y(), skill, luck, option, excellent, -1)
		else:
			Map(self.get_map()).drop_serial(section, index, level, dur, self.get_x(), self.get_y(), skill, luck, option, excellent, player.get_index())

	#
	# Talk
	#
	def talk(self, player, text):
		Server.npc_message(player.get_index(), self.get_index(), text)