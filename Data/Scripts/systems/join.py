# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.config import *

#
# Handler class
#
def system_join(index):
	if index < Server.player_start: 
		return
	player = Player(index)
	if player.is_admin():
		if config.systems.join_global_staff:
			Server.broadcast_announcement('%s entrou no servidor %s' % (player.get_name(), Server.server_name))	
	else:
		player.announce('Ol� %s, seja bem-vindo ao %s!' % (player.get_name(), config.options.server_name))
		if config.systems.join_global_normal:
			Server.send_message_all('%s entrou no jogo!' % player.get_name())	

Events.register('player.join', system_join)