# -*- coding: UTF-8 -*-

from wolfulus import *
from utils.player import *
from utils.monster import *
from utils.config import *
from utils.timer import *
from utils.thread import *
from utils.database import *

import threading
import random

# @threaded
def unban_timer():
	db = database.get('muonline')
	Console.debug('Desbanindo contas e chars')
	# Desbane os chars bloqueados
	cursor = db.cursor()
	cursor.execute("UPDATE Character SET CtlCode = 0 WHERE CtlCode = 1 AND %s IS NOT NULL AND %s < GETDATE()" % (config.columns.banchar_timer, config.columns.banchar_timer))
	cursor.execute("UPDATE MEMB_INFO SET bloc_code = 0 WHERE bloc_code = 1 AND %s IS NOT NULL AND %s < GETDATE()" % (config.columns.ban_timer, config.columns.ban_timer))
	cursor.close()
	# Salva
	db.commit()

timer.interval(unban_timer, 60000)