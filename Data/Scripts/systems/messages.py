# -*- coding: UTF-8 -*-

from wolfulus import *
from utils.player import *
from utils.monster import *

import random

#
# Auto message system
#
class AutoMessage(object):

	#
	# Constructor
	#
	def __init__(self):
		self.current = 0
		messages = dict()
		for msg in config.data.messages.messages:
			if msg[0] in messages:
				messages[msg[0]].append(msg[1])
			else:
				messages[msg[0]] = [msg[1]]
		self.messages = messages.values()
		timer.interval(self.send_message, 1000 * config.messages.interval)

	#
	# Timer
	#
	def send_message(self):
		if not config.messages.enabled:
			return
		message = self.messages[self.current % len(self.messages)]
		for line in message:
			Server.send_announcement_all(line)
		self.current = self.current + 1


automsg = AutoMessage()