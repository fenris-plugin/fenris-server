# -*- coding: UTF-8 -*-

from wolfulus import *
from utils.player import *
from utils.monster import *
from utils.config import *

import random

def player_item_drop(pid, x, y, ipos):

	player = Player(pid)
	item = player.get_inventory().get_item(ipos)

	for box in config.data.boxes.boxes:
		
		item_section = int(box[0])
		item_index = int(box[1])
		item_level = int(box[2])
		item_fireworks = int(box[3])
		item_boxname = box[4]

		if not (item.get_section() == item_section and item.get_index() == item_index):
			continue

		if item_level >= 0 and item_level <= 15:
			if not (item.get_level() == item_level):
				continue

		if item_fireworks > 0:
			Map(player.get_map()).fireworks(x, y, item_fireworks)

		drops = getattr(config.data.boxes, item_boxname)

		val = random.uniform(0, sum(int(d[12]) for d in drops))
		
		current = 0
		for drop in drops:

			current = current + int(drop[12])
			if val >= current:
				continue

			ex = 0
			if int(drop[6]) != 0:
				ex = ex | 1
			if int(drop[7]) != 0:
				ex = ex | 2
			if int(drop[8]) != 0:
				ex = ex | 4
			if int(drop[9]) != 0:
				ex = ex | 8
			if int(drop[10]) != 0:
				ex = ex | 16
			if int(drop[11]) != 0:
				ex = ex | 32

			Map(player.get_map()).drop_serial(int(drop[0]), int(drop[1]), int(drop[2]), 255, x, y, int(drop[4]), int(drop[5]), int(drop[3]), ex, player.get_index())
			break

		player.delete_item(ipos)
		return True

	return False

Events.register('player.drop_item', player_item_drop)