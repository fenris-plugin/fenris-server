# -*- coding: UTF-8 -*-

from wolfulus import *
from utils.player import *
from utils.config import *


def join_check(pid):
	player = Player(pid)
	if config.options.server_vip:
		if not player.is_vip():
			player.message('[Sistema] Este servidor est� liberado somente para VIPs.')
			player.announce('[SERVIDOR VIP]')
			player.announce('Desculpe %s, somente VIPs podem conectar neste servidor.' % player.get_name())
			player.set_closetype(2)
			player.set_closecount(6)
	elif config.options.server_max_resets > 0:
		if player.get_resets() > config.options.server_max_resets:
			player.message('[Sistema] Este servidor est� liberado somente para iniciantes.')
			player.announce('[SERVIDOR INICIANTE]')
			player.announce('Desculpe %s, este servidor � liberado somente' % player.get_name())
			player.announce('para personagens com at� %d resets.' % config.options.server_max_resets)
			player.set_closetype(2)
			player.set_closecount(6)


Events.register('player.join', join_check)