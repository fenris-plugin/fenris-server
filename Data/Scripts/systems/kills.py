# -*- coding: UTF-8 -*-

#
# Imports
#
from wolfulus import *
from utils.player import *
from utils.config import *
from utils.database import *

pks = dict()
heroes = dict()

#
# Handler class
#
def kill_player(index1, index2):
	
	player1 = Player(index1)
	player2 = Player(index2)

	if player2.get_pklevel() <= 4:
		if not player1.get_name() in pks:
			pks[player1.get_name()] = 1
		else:
			pks[player1.get_name()] = pks[player1.get_name()] + 1
	else:
		if not player1.get_name() in heroes:
			heroes[player1.get_name()] = 1
		else:
			heroes[player1.get_name()] = heroes[player1.get_name()] + 1

def kill_player_left(index1, name, account):

	if name in pks:
		db = database.get('muonline')
		if not isinstance(config.columns.pk_count, list):
			cursor = db.cursor()
			cursor.execute("UPDATE Character SET %s = %s + ? WHERE Name = ?" % (config.columns.pk_count, config.columns.pk_count), [pks[name], name])
			cursor.commit()
			cursor.close()
		else:
			for column in config.columns.pk_count:
				cursor = db.cursor()
				cursor.execute("UPDATE Character SET %s = %s + ? WHERE Name = ?" % (column, column), [pks[name], name])
				cursor.commit()
				cursor.close()
		del pks[name]

	if name in heroes:
		db = database.get('muonline')
		if not isinstance(config.columns.hero_count, list):
			cursor = db.cursor()
			cursor.execute("UPDATE Character SET %s = %s + ? WHERE Name = ?" % (config.columns.hero_count, config.columns.hero_count), [heroes[name], name])
			cursor.commit()
			cursor.close()
		else:
			for column in config.columns.hero_count:
				cursor = db.cursor()
				cursor.execute("UPDATE Character SET %s = %s + ? WHERE Name = ?" % (column, column), [heroes[name], name])
				cursor.commit()
				cursor.close()
		del heroes[name]

Events.register('player.kill', kill_player)
Events.register('player.left', kill_player_left)