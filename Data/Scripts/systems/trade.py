# -*- coding: UTF-8 -*-

from wolfulus import *
from utils.player import *
from utils.monster import *
from utils.config import *

import random

def move_trade(pid, pos):
	player = Player(pid)
	inventory = player.get_inventory()
	item = inventory.get_item(pos)
	for items in config.data.tradeblock.items:
		if item.is_item(int(items[0]), int(items[1])):
			if int(items[2]) == -1:
				player.message('Voc� n�o pode negociar este item.')
				return False # Bloqueia o movimento do item
			else:
				if item.get_level() == int(items[2]):
					player.message('Voc� n�o pode negociar este item.')
					return False # Bloqueia o movimento do item
	return True # Libera o movimento do item

Events.register('player.move_to_trade', move_trade)